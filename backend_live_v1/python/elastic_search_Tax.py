
# coding: utf-8

# In[17]:

from os import popen
import requests
import subprocess
import time
import urllib
import urllib2
from elasticsearch import Elasticsearch
import json 
from datetime import datetime
from pymongo import MongoClient
from pprint import pprint
from gingerit.gingerit import GingerIt


production_ip = '52.77.59.43'
#elasticsearch_server_address = ''
#def start_server():
#   try:
#        subprocess.Popen(elasticsearch_server_address)
#        time.sleep(10)
#        res = requests.get("http://localhost:9200")
#        print res
#   except urllib2.HTTPError:
#        pass
    
def get_elastic_search():
    es = Elasticsearch([{'host':'localhost', 'port':9200}])
    return es

#start_server()
es = get_elastic_search()
SUGGESTIVE_INDEX_NAME = 'suggestive_phrases'

def index_suggestive_words(filename):
    
    global SUGGESTIVE_INDEX_NAME
    if es.indices.exists(SUGGESTIVE_INDEX_NAME):
        es.indices.delete(index = SUGGESTIVE_INDEX_NAME)
    i = 1
    with open(filename, 'r') as f:
        for line in f:
            data = {}
            data['name'] = line.rstrip()   #remove the new line signature
            json_data = json.dumps(data)
            i += 1
            es.index(SUGGESTIVE_INDEX_NAME, 'key-words', json_data)
#             print json_data
            
#index_suggestive_words("/home/ec2-user/elastic_search/ElasticSearch/law_words.txt")
            
#example sentence to correctify using gingerit library
text = 'The smelt of fliwers bring bac memories.'
parser = GingerIt()
print parser.parse(text)['result']

def insert_word_for_indexing(word):
    print "here sentence :: " + word
    data = {}
    data['name'] = word
    json_data = json.dumps(data)
    return es.index(SUGGESTIVE_INDEX_NAME, 'key-words', json_data)

def add_phrase_after_submit(sentence):
    global parser
    if es.indices.exists(SUGGESTIVE_INDEX_NAME):
        query_result = es.search(index= SUGGESTIVE_INDEX_NAME, body={"query": {"match" : { "name" : sentence }}})
        print query_result
        result = query_result['hits']['hits']
        output_num = len(result)
        grammerly_right_sentence = parser.parse(sentence)['result']
        if(output_num == 0):
            insert_word_for_indexing(grammerly_right_sentence)
        else:
            top_scorer_phrase = result[0]['_source']['name']
            if( sentence != top_scorer_phrase):
                insert_word_for_indexing(sentence)
        
    
def suggest_word(word):
    fuzziness_value = 2
    length  = len(word)
    if(length < 5):
        fuzziness_value = 1
    data = es.search(index= SUGGESTIVE_INDEX_NAME, body={"query": {"fuzzy" : { "name":{"value": word, "fuzziness":fuzziness_value,"max_expansions" : 100}}}})
    result = data['hits']['hits']
#     output_num=len(result)
    return result


#code to document query using elastic search
def get_mongo_client():
    dbName = "pensieve"
    connString = 'mongodb://pensieve_user:'+ urllib.quote_plus("Pixel%0909") +'@'+production_ip+':27017/'+dbName+'?authMechanism=SCRAM-SHA-1'
    client = MongoClient(connString)
    return client

def get_database():
    client = get_mongo_client()
    try:
        db = client.pensieve
        return db
    except ConnectionFailure (e):
        sys.stderr.write("Could not open connection to mongodb :: %s"%e)
        return null

def get_tax_case_judis_collection():
#     db.collection_names()
    db = get_database()
    collection = db.taxman_db
   # collection = db.scr_cases_judis
#    cursor = collection.find({}).limit(2)
#    for doc in cursor:
#        print doc
    return collection
# Getting cursor to documents
#get_scr_case_judis_collection()
def document_generator():
    collection = get_tax_case_judis_collection()
    cursor = collection.find()
    for document in cursor:
        yield document


#SCR_INDEX_NAME = 'scr_cases_judis_new'
tax_INDEX_NAME = 'tax_hc_cases'

#SCR_DOCUMENT = 'scr-datas'
tax_DOCUMENT = 'tax-datas-new'

def create_tax_index():
    global tax_INDEX_NAME
    global tax_DOCUMENT
    if es.indices.exists(tax_INDEX_NAME):
        es.indices.delete(index = tax_INDEX_NAME)
    mapping = {
      "mappings":{
        tax_DOCUMENT:{
          "properties":{
            "date_of_judgement":    { "type": "text", "format" : "dd/MM/yyy"  }, 
            "judge":     { "type": "text"  }, 
            "case_title":      { "type": "text" },
            "corpus_id" : {"type" : "integer"},
            "judgement": {"type" : "text"},
            "citation" : {"type" : "text"},
            "subject" : {"type" : "text"}
          }
        }
      }
    }
    es.indices.create(index = tax_INDEX_NAME, ignore = 400, body = mapping,request_timeout = 6000)
    
def index_tax_documents():
    global tax_INDEX_NAME
    global tax_DOCUMENT
    i = 1
    doc_gen = document_generator()
    for doc in doc_gen:
        data = {}
#        date_of_judgement = doc['date_of_judgement']
#        year_string = date_of_judgement.split('/')  #array of d-m-y
#        data['date_of_judgement'] = year_string[-1]
#       data['date_of_judgement'] = doc['date_of_judgement']
#       data['judge'] = doc['judge']
        data['case_title'] = doc['title']
        data['corpus_id'] = doc['corpus_id']
        data['judgement'] = doc['judgement']
#       data['citation'] = doc['citation']
#       data['subject'] = doc['subject']

        duplicate_ids = [26477, 26476, 26475, 26440, 26439, 26438, 26043, 25836, 25839, 25834, 25837, 25825, 25828, 25824, 25827, 25670, 25643, 25645, 25644, 25606, 25602, 19788, 14611, 20832, 13919, 24915, 24918, 24914, 24917, 24912, 24916, 24899, 24906, 24898, 24905, 24901, 24908, 25440, 25443, 21522, 24494, 24373, 24372, 24374, 24320, 17919, 24309, 24304, 24297, 20870, 24046, 18520, 14901, 24098, 24107, 24094, 24084, 24091, 23982, 23974, 18129, 23907, 23849, 23847, 23806, 23804, 23803, 24308, 20595, 23484, 23478, 23476, 22074, 22084, 22094, 23447, 20869, 23186, 23003, 22834, 22833, 22773, 22772, 22767, 22766, 22765, 22763, 22761, 22744, 22671, 22651, 22650, 25414, 22609, 22572, 22436, 22422, 22425, 22424, 22405, 22392, 22385, 22386, 22377, 22382, 22381, 22380, 22379, 22378, 22293, 22222, 22221, 22139, 22149, 22158, 22136, 22143, 22155, 22122, 22130, 22146, 22150, 22069, 22079, 22088, 22075, 22085, 22095, 22072, 22082, 22092, 22071, 22081, 22091, 22068, 22076, 22078, 22014, 20834, 22384, 21934, 21862, 16938, 21840, 24491, 21757, 21756, 21755, 22036, 22042, 22054, 21619, 21568, 21530, 21531, 21443, 15925, 21345, 21344, 21343, 16840, 19904, 21271, 21270, 21269, 16871, 21172, 21073, 16545, 20971, 20864, 20862, 20860, 20856, 20871, 20859, 20857, 20868, 20867, 20866, 20876, 20831, 20830, 20820, 20817, 20729, 20670, 15670, 22437, 20606, 20605, 20601, 20594, 20582, 20581, 20580, 20533, 20473, 20464, 20463, 20460, 20454, 20452, 20397, 20280, 20279, 20281, 20267, 20260, 20266, 20269, 20273, 20221, 20223, 20220, 23479, 20873, 20124, 20087, 20084, 22762, 20044, 20978, 14712, 19872, 15801, 19664, 22402, 22738, 19537, 17986, 17992, 23983, 22418, 23848, 19225, 19228, 19231, 19226, 19229, 19232, 19208, 19210, 19218, 19203, 19207, 19217, 22035, 22053, 19161, 19159, 19154, 19153, 21521, 25441, 25444, 22771, 21020, 21511, 20095, 18554, 18553, 18543, 18544, 18531, 18530, 18501, 18499, 18500, 18472, 18471, 18462, 18450, 18436, 18435, 18437, 18434, 18428, 18403, 18383, 18385, 18384, 18382, 20270, 18359, 18355, 18342, 18341, 14675, 24081, 24088, 15946, 23808, 22138, 22148, 22157, 18198, 18191, 18190, 18193, 23971, 18161, 18164, 18146, 18145, 18142, 22426, 18022, 18013, 18012, 18014, 17995, 17993, 17972, 17971, 17958, 17961, 17941, 17939, 17928, 17926, 17916, 21523, 17888, 19041, 16113, 23146, 18564, 17271, 18015, 22866, 22871, 17124, 20399, 18516, 17960, 17003, 14878, 16986, 16961, 16939, 16931, 22220, 22077, 22089, 16853, 16762, 15724, 16673, 16644, 15230, 18199, 25622, 16289, 22865, 16265, 22401, 16160, 15640, 16139, 16143, 16125, 20818, 16111, 20822, 25649, 15985, 20521, 17940, 15898, 15853, 22423, 15855, 15840, 15839, 15786, 15757, 15753, 15752, 24888, 24902, 15727, 15713, 15696, 15684, 15652, 15639, 15638, 18358, 15604, 15601, 15591, 15579, 13845, 24300, 19804, 19807, 18521, 15459, 19805, 19808, 22433, 15255, 16842, 20398, 20865, 15765, 16851, 16854, 22391, 14597, 14600, 20875, 20863, 14945, 14855, 22995, 18158, 19170, 14714, 14703, 14698, 14693, 14695, 14678, 20472, 14663, 14656, 14647, 16536, 14648, 14636, 14624, 14622, 14610, 14599, 14593, 14589, 14583, 24083, 24090, 14539, 14529, 23483, 20819, 23685, 22435, 24298, 14694, 14164, 22134, 22141, 22153, 25442, 25445, 13854, 13852, 13844, 13841, 13811, 13810, 13809, 22039, 22048, 22052, 13789, 13788, 24487, 14616, 19505, 13615, 14682, 16624, 18451, 13461, 20453, 18209, 16538, 18413, 23985, 20600, 23805, 21534, 15483, 15976, 18021, 22427, 22383, 23042, 20522, 24303, 11587, 17996, 18207, 24305, 20088, 12274, 15858, 17927, 22408, 22743, 22133, 22140, 22152, 20583, 10804, 23984, 16590, 21763, 24085, 24092, 12506, 24889, 24904, 12712, 18552, 22760, 18363, 21267, 17954, 14594, 17973, 16546, 17490, 18547, 22434, 22087, 22096, 20874, 24307, 21145, 24295, 18157, 24082, 24089, 17959, 18404, 15948, 16898, 15956, 17158, 23807, 24047, 16625, 24422, 26037, 20459, 20972, 14697, 23995, 22648, 13806, 14709, 20624, 14536, 16856, 21897, 20295, 17317, 20858, 16855, 14651, 21272, 19169, 20396, 17915, 18159, 19510, 20625, 21829, 24302, 22164, 17370, 19500, 8583, 21533, 19202, 19206, 19216, 21412, 9544, 24045, 14707, 20872, 24044, 18463, 21764, 19483, 19485, 19491, 15118, 16450, 19412, 18412, 20417, 15860, 16393, 22037, 22046, 22050, 14608, 15682, 22043, 22049, 22055, 23802, 17962, 16794, 15734, 15683, 13853, 14552, 20833, 19803, 19806, 20861, 18545, 17957, 14615, 22828, 21810, 15806, 24306, 18473, 13843, 18465, 20416, 18162, 17901, 22137, 22144, 22156, 14715, 24301, 21830, 26474, 24488, 19066, 17994, 19155, 19205, 19209, 19215, 16623, 19201, 19204, 19214, 20526, 22038, 22047, 22051, 22259, 24080, 24087, 18430, 22045, 22056, 23477, 23832, 14567, 25373, 22996, 18515, 17990, 19676, 15875, 14631, 23801, 20212, 21268, 25594, 13807, 13855, 18466, 24299, 20272, 18160, 25835, 25838, 22403, 22135, 22142, 22154, 21375, 20271, 16247, 21746, 15755, 15585, 22073, 22083, 22093, 24887, 24903, 16169, 21532, 23981, 23166, 15235, 20268, 15766, 19224, 19227, 19230, 22742, 14686, 20458, 18467, 22404, 21512, 14696, 18470, 24100, 19849, 22406, 24490, 18416, 24900, 24907, 14623, 21075, 22649, 23185, 22147, 22151, 16266, 3101, 23716, 22070, 22080, 22090, 18163, 14602, 21747, 24296, 14496, 20977, 18546, 19015, 16124, 22407, 21074, 20607]
#        data['combined_key'] = doc['case_title'] + " " + doc['judge'] + " " + year_string[-1]
        json_data = json.dumps(data)
        if data['corpus_id'] in duplicate_ids:
            print "duplicate :: " + str(data['corpus_id'])
            continue
        es.index(tax_INDEX_NAME, tax_DOCUMENT , json_data)
        print i
        i += 1
    print i
create_tax_index()
index_tax_documents()

def query_on_title(title):
    global es
    search_results = []
    query_result = es.search(index=tax_INDEX_NAME, body={
        "query" : {"match" : {
                "case_title": {
                    "query" : title,
                }
            }}
    })
    results = query_result['hits']['hits']
    return results


# In[19]:


#     print len(results)
query_on_title("PATIYALA HINDU ATMA")


# In[3]:
# In[4]:
suggest_word("hossesion")
