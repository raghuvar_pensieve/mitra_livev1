var client = require("./esClient");
var cases = require("./cases");

exports.findByCaseTitleES = function(req, res) {
    // console.log(req.query.title)

    var title = req.body.title;

    client.search({
      index: 'scr_cases_judis_new',
      type: 'scr-datas-new',
	size : 50,
	_source : ["case_id"],
      body: {
        query: {
		match : {
			"case_title": title
		}
	}
      }
    },function (error, response,status) {
	if(error){
                        res.send({"error and title ::: " : error });
                }
                else{
                        var case_ids = [];
                        var datas = response.hits.hits;
                        query_result = [];
                        datas.forEach(function(value){
                                data = {};
                                data["case_id"]  = value._source.case_id;
                                query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
                                case_ids.push(value._source.case_id);
                        });
                        console.log(query_result);
                        // datas.forEach(data=>
                        //                 case_ids.push(data._source.case_id)
                        // )
                      cases.add_feedback(title, query_result, "essearch_scr_title", function(result){
				res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
                        });
		//	 res.send({"search_id":search_id,"search_query":title,"similar_cases":case_ids});
                        // res.send({"similar_cases" : case_ids});
	             }
 });
};

exports.findByCaseJudgement = function(req, res) {
    // console.log(req.query.title)

    var judgement = req.body.judgement;
    console.log(" judgement query :: " + judgement);

    client.search({
        index: 'scr_cases_judis_new',
        type: 'scr-datas-new',
        size : 50,
	    _source : ["case_id"],
        body: {
		query: {
        	  match: { "judgement": judgement },
       		},
       }
    },function (error, response,status) {
        if(error){
            res.send({"error and title ::: " : judgement });
        }
        else{
            var case_ids = [];
            var datas = response.hits.hits;
            query_result = [];
            datas.forEach(function(value){
                data = {};
                data["case_id"]  = value._source.case_id;
                query_result.push({"case_id": value._source.case_id, "relevancy" : value._score  } ) ;
                case_ids.push(value._source.case_id);
            });
            console.log(query_result);
            cases.add_feedback(judgement, query_result, "essearch_scr_judgement", function(result){
		res.send({"search_id":result, "search_query":judgement,"similar_cases":case_ids});
            });
            // datas.forEach(data=>
            //     case_ids.push(data._source.case_id)
            // )
            // res.send({"similar_cases" : case_ids});

//            res.send({"search_id":search_id,"search_query":judgement,"similar_cases":case_ids});

        }
    });
};


exports.esSearchMumbaiJudgement = function(req, res) {
    console.log(req.body);
    console.log("judgement in es :: " + req.body.judgement);
    var judgement = req.body.judgement;

    client.search({
        index:'mumbai_hc_cases',
        type: 'mumbai-datas-new',
        size : 50,
        _source : ["case_id"],
        body: {
            query: {
                match: { "judgement": judgement }
            },
        }
    },function (error, response,status) {
        if(error){
            res.send({"error in judgement ::: " : error });
        }
        else{
            var case_ids = [];
            var datas = response.hits.hits;
            query_result = [];
            datas.forEach(function(value){
                data = {};
                data["case_id"]  = value._source.case_id;
                query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
                case_ids.push(value._source.case_id);
            });
            console.log(query_result);
            // datas.forEach(data=>
            //                 case_ids.push(data._source.case_id)
            // )
            cases.add_feedback(judgement, query_result, "essearch_judgement_mumbai", function(result){
                res.send({"search_id":result, "search_query":judgement,"similar_cases":case_ids});
            });
            //	 res.send({"search_id":search_id,"search_query":title,"similar_cases":case_ids});
            // res.send({"similar_cases" : case_ids});
        }
    });
};

exports.esSearchMumbaiTitle = function(req, res) {
    console.log(req.body);
    console.log("mumbai Title in es :: " + req.body.title);
    var title = req.body.title;

    client.search({
        index: 'mumbai_hc_cases',
        type: 'mumbai-datas-new',
        size : 50,
        _source : ["case_id"],
        body: {
            query: {
                match: { "case_title": title }
            },
        }
    },function (error, response,status) {
        if(error){
            res.send({"error in title ::: " : error });
        }
        else{
            var case_ids = [];
            var datas = response.hits.hits;
            query_result = [];
            datas.forEach(function(value){
                data = {};
                data["case_id"]  = value._source.case_id;
                query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
                case_ids.push(value._source.case_id);
            });
            console.log(query_result);
            // datas.forEach(data=>
            //                 case_ids.push(data._source.case_id)
            // )
            cases.add_feedback(title, query_result, "essearch_title_mumbai", function(result){
                res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
            });
            //	 res.send({"search_id":search_id,"search_query":title,"similar_cases":case_ids});
            // res.send({"similar_cases" : case_ids});
        }
    });
};



exports.esSearchDelhiJudgement = function(req, res) {
    console.log(req.body);
    console.log("judgement in es :: " + req.body.judgement);
    var judgement = req.body.judgement;

    client.search({
        index: 'delhi_hc_cases',
        type: 'delhi-datas-new',
        size : 50,
        _source : ["case_id"],
        body: {
            query: {
                match: { "judgement": judgement }
            },
        }
    },function (error, response,status) {
        if(error){
            res.send({"error in judgement ::: " : error });
        }
        else{
            var case_ids = [];
            var datas = response.hits.hits;
            query_result = [];
            datas.forEach(function(value){
                data = {};
                data["case_id"]  = value._source.case_id;
                query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
                case_ids.push(value._source.case_id);
            });
            console.log(query_result);
            // datas.forEach(data=>
            //                 case_ids.push(data._source.case_id)
            // )
            cases.add_feedback(judgement, query_result, "essearch_judgement_delhi", function(result){
                res.send({"search_id":result, "search_query":judgement,"similar_cases":case_ids});
            });
            //	 res.send({"search_id":search_id,"search_query":title,"similar_cases":case_ids});
            // res.send({"similar_cases" : case_ids});
        }
    });
};

exports.esSearchDelhiTitle = function(req, res) {
    console.log(req.body);
    console.log("delhi Title in es :: " + req.body.title);
    var title = req.body.title;

    client.search({
        index: 'delhi_hc_cases',
        type: 'delhi-datas-new',
        size : 50,
        _source : ["case_id"],
        body: {
            query: {
                match: { "case_title": title }
            },
        }
    },function (error, response,status) {
        if(error){
            res.send({"error in title ::: " : error });
        }
        else{
            var case_ids = [];
            var datas = response.hits.hits;
            query_result = [];
            datas.forEach(function(value){
                data = {};
                data["case_id"]  = value._source.case_id;
                query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
                case_ids.push(value._source.case_id);
            });
            console.log(query_result);
            // datas.forEach(data=>
            //                 case_ids.push(data._source.case_id)
            // )
            cases.add_feedback(title, query_result, "essearch_title_delhi", function(result){
                res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
            });
            //	 res.send({"search_id":search_id,"search_query":title,"similar_cases":case_ids});
            // res.send({"similar_cases" : case_ids});
        }
    });
};



exports.esSearchTaxJudgement = function(req, res) {
    console.log(req.body);
    console.log("Title in es :: " + req.body.title);
    var judgement = req.body.judgement;

    client.search({
        index: 'tax_hc_cases',
        type: 'tax-datas-new',
        size : 50,
        _source : ["case_id"],
        body: {
            query: {
                match: { "judgement": judgement }
            },
        }
    },function (error, response,status) {
        if(error){
            res.send({"error and title ::: " : judgement });
        }
        else{
            var case_ids = [];
            var datas = response.hits.hits;
            query_result = [];
            datas.forEach(function(value){
                data = {};
                data["case_id"]  = value._source.case_id;
                query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
                case_ids.push(value._source.case_id);
            });
            console.log(query_result);
            // datas.forEach(data=>
            //                 case_ids.push(data._source.case_id)
            // )
            cases.add_feedback(title, query_result, "essearch_judgement_tax", function(result){
                res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
            });
            //	 res.send({"search_id":search_id,"search_query":title,"similar_cases":case_ids});
            // res.send({"similar_cases" : case_ids});
        }
    });
};



exports.findByTaxDateRange = function(req, res) {
    // console.log(req.query.title)

    var date1 = req.query.start_date;
    var date2 = req.query.end_date;

    client.search({
        index: 'scr_cases_judis_new',
        type: 'scr-datas-new',
        size : 50,
        _source : ["case_id"],
        body: {
            query: {
                filtered :{
                    query: {
                        match_all : { }
                    },
                    filter :{
                        range : { "date" : { 'gte' : date1, 'lte' : date2}}
                    }
                }
            },
        }
    },function (error, response,status) {
        if (error){
            console.log("search error: "+error);
            res.send({"response" : "unsuccessful"}) ;
        }
        else{
            var case_ids = [];
            var datas = response.hits.hits;
            query_result = [];
            datas.forEach(function(value){
                data = {};
                data["case_id"]  = value._source.case_id;
                query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
                case_ids.push(value._source.case_id);
            });
            console.log(query_result);
            // datas.forEach(data=>
            //                 case_ids.push(data._source.case_id)
            // )
            cases.add_feedback(title, query_result, "essearch_year_tax", function(result){
                res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
            });
            //	 res.send({"search_id":search_id,"search_query":title,"similar_cases":case_ids});
            // res.send({"similar_cases" : case_ids});
        }
    });
};

exports.esSearchTaxTitle = function(req, res) {
    console.log(req.body);
    console.log("Title in es :: " + req.body.title);
    var title = req.body.title;

    client.search({
        index: 'tax_hc_cases',
        type: 'tax-datas-new',
        size : 50,
        _source : ["case_id"],
        body: {
            query: {
                match: { "case_title": title }
            },
        }
    },function (error, response,status) {
        if(error){
            res.send({"error and title ::: " : title });
        }
        else{
            var case_ids = [];
            var datas = response.hits.hits;
            query_result = [];
            datas.forEach(function(value){
                data = {};
                data["case_id"]  = value._source.case_id;
                query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
                case_ids.push(value._source.case_id);
            });
            console.log(query_result);
            // datas.forEach(data=>
            //                 case_ids.push(data._source.case_id)
            // )
            cases.add_feedback(title, query_result, "essearch_year_tax", function(result){
                res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
            });
            //	 res.send({"search_id":search_id,"search_query":title,"similar_cases":case_ids});
            // res.send({"similar_cases" : case_ids});
        }
    });
};



