var client = require("./esClient");
var cases = require("./cases");
var mongo = require('mongodb');
var assert = require('assert'); 
var Server = mongo.Server,
	Db = mongo.Db,
	BSON = mongo.BSONPure;
var mongoHost = "52.77.59.43"; //production
var dbName = "pensieve";
var collection = "scr_cases_judis_new";
var moment = require('moment');
var server = new Server(mongoHost, 27017, {auto_reconnect: true});

db = new Db(dbName, server); //test
db.open(function(err, db) {
	
	if(!err) {
		console.log("Connected to 'pensieve' database");
		db.authenticate('pensieve_user', 'Pixel%0909', function(err, result) {
			assert.equal(true, result);

			//db.close();
		});
		db.collection(collection, {strict:true}, function(err, collection) {
			if (err) {
				console.log("The 'account' collection doesn't exist");
			}
			else{
				console.log("esr line: 29");
			}
		});

	}
});

exports.esSearchMumbaiJudgement = function(req, res) {
	
	var judgement = req.body.postbody.judgement;

	client.search({
		index: 'mumbai_hc_cases',
		type: 'mumbai-datas-new',
		size : 50,
		_source : ["case_id"],
		body: {
			query: {
				match: { "judgement": judgement }
			},
		}
	},function (error, response,status) {
		if(error){
			var case_ids = [1];
			res.send({"similar_cases" : case_ids});	
		}
		else{
			if(req.body.username!=undefined){
				db.collection('my_account', function(err, collection) {
						  if(err)  {
							throw err;
						  }
						  else{
							var searchObject = {'search_query': judgement, timestamp: moment().format()};
							
							collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
								if(err){
									throw err;
								}
								else{
									console.log(item);
								}
							});
						  }
				});
			}
			var case_ids = [];
			var datas = response.hits.hits;
			query_result = [];
			datas.forEach(function(value){
				data = {};
				data["case_id"]  = value._source.case_id;
				query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
				case_ids.push(value._source.case_id);
			});
			
			cases.add_feedback(judgement, query_result, "essearch_judgement_mumbai", function(result){
				res.send({"search_id":result, "search_query":judgement,"similar_cases":case_ids});
			});
			
		}
	});
};

exports.esSearchMumbaiTitle = function(req, res) {
	
	var title = req.body.postbody.title;
    
	client.search({
		index: 'mumbai_hc_cases',
		type: 'mumbai-datas-new',
		size : 50,
		_source : ["case_id"],
		body: {
			query: {
				match: { "case_title": title }
			},
		}
	},function (error, response,status) {
		if(error){
			var case_ids = [1];
			res.send({"similar_cases" : case_ids});
		}
		else{
			if(req.body.username!=undefined){
				db.collection('my_account', function(err, collection) {
						  if(err)  {
							throw err;
						  }
						  else{
							var searchObject = {'search_query': title, timestamp: moment().format()};
							
							collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
								if(err){
									throw err;
								}
								else{
									console.log(item);
								}
							});
						  }
				});
			}

			var case_ids = [];
			var datas = response.hits.hits;
			query_result = [];
			datas.forEach(function(value){
				data = {};
				data["case_id"]  = value._source.case_id;
				query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
				case_ids.push(value._source.case_id);
			});
			console.log(case_ids);
			cases.add_feedback(title, query_result, "essearch_title_mumbai", function(result){
				res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
			});
			
		}
	});
};



exports.esSearchDelhiJudgement = function(req, res) {
	
	var judgement = req.body.postbody.judgement;

	client.search({
		index: 'delhi_hc_cases',
		type: 'delhi-datas-new',
		size : 50,
		_source : ["case_id"],
		body: {
			query: {
				match: { "judgement": judgement }
			},
		}
	},function (error, response,status) {
		if(error){
			var case_ids = [1,100];		
			res.send({"similar_cases" : case_ids});	
		}
		else{
			if(req.body.username!=undefined){
				db.collection('my_account', function(err, collection) {
						  if(err)  {
							throw err;
						  }
						  else{
							var searchObject = {'search_query': judgement, timestamp: moment().format()};
							
							collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
								if(err){
									throw err;
								}
								else{
									console.log(item);
								}
							});
						  }
				});
			}

			var case_ids = [];
			var datas = response.hits.hits;
			query_result = [];
			datas.forEach(function(value){
				data = {};
				data["case_id"]  = value._source.case_id;
				query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
				case_ids.push(value._source.case_id);
			});
			
			cases.add_feedback(judgement, query_result, "essearch_judgement_delhi", function(result){
				res.send({"search_id":result, "search_query":judgement,"similar_cases":case_ids});
			});
			
		}
	});
};

exports.esSearchDelhiTitle = function(req, res) {
	
	var title = req.body.title;

	client.search({
		index: 'delhi_hc_cases',
		type: 'delhi-datas-new',
		size : 50,
		_source : ["case_id"],
		body: {
			query: {
				match: { "case_title": title }
			},
		}
	},function (error, response,status) {
		if(error){
			var case_ids = [1];
			res.send({"similar_cases" : case_ids});
		}
		else{
			if(req.body.username!=undefined){
				db.collection('my_account', function(err, collection) {
						  if(err)  {
							throw err;
						  }
						  else{
							var searchObject = {'search_query': title, timestamp: moment().format()};
							console.log(searchObject);
							collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
								if(err){
									throw err;
								}
								else{
									console.log(item);
								}
							});
						  }
				});
			}
			var case_ids = [];
			var datas = response.hits.hits;
			query_result = [];
			datas.forEach(function(value){
				data = {};
				data["case_id"]  = value._source.case_id;
				query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
				case_ids.push(value._source.case_id);
			});
			
			cases.add_feedback(title, query_result, "essearch_title_delhi", function(result){
				res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
			});
			
		}
	});
};


exports.esSearchUSJudgement = function(req, res) {
	
	var judgement = req.body.postbody.judgement;
	console.log("Us: " + judgement);
	client.search({
		index: 'us_data',
		type: 'us-datas',
		size : 50,
		body: {
			query: {
				match: { "html_with_citations": judgement }
			},
		}
	},function (error, response,status) {
		if(error){
			var case_ids = [1,100];
			res.send({"similar_cases" : case_ids});   
		}
		else{
            if(req.body.username!=undefined){
				db.collection('my_account', function(err, collection) {
						  if(err)  {
							throw err;
						  }
						  else{
							var searchObject = {'search_query': judgement, timestamp: moment().format(), 'type': 'US_judgement'};
							
							collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
								if(err){
									throw err;
								}
								else{
									//console.log(item);
								}
							});
						  }
				});
			}

			var case_ids = [];
			var case_data = [];
			var datas = response.hits.hits;
			query_result = [];
			datas.forEach(function(value){								
				query_result.push({"case_id": value._source.case_id_source, "relevancy" : value._score });
				case_ids.push(value._source.case_id_source);
				case_data.push({"_source":value._source})
			});
			cases.add_feedback(judgement, query_result, "essearch_judgement_us", function(result){		
				res.send({"search_id":result, "search_query":judgement,"case_data":case_data});
			});		

			
			
		}
	});
};

exports.esSearchUSTitle = function(req, res) {
	
	var title = req.body.postbody.title;

	client.search({
		index: 'us_data',
		type: 'us-datas',
		size : 50,
		body: {
			query: {
				match: { "case_title": title }
			},
		}
	},function (error, response,status) {
		if(error){			
			var case_ids = [1];
			res.send({"similar_cases" : case_ids});
		}
		else{
			
			if(req.body.username!=undefined){
				db.collection('my_account', function(err, collection) {
					  if(err)  {
						throw err;
					  }
					  else{
						var searchObject = {'search_query': title, timestamp: moment().format(), 'type': 'US_title'};
						
						collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
							if(err){
								throw err;
							}
							else{
								console.log(item);
							}
						});
					  }
				});    
			}
			
			var case_ids = [];
			var case_data = [];
			var datas = response.hits.hits;
			query_result = [];
			datas.forEach(function(value){								
				query_result.push({"case_id": value._source.case_id_source, "relevancy" : value._score });
				case_ids.push(value._source.case_id_source);
				case_data.push({"_source":value._source})
			});
			cases.add_feedback(title, query_result, "essearch_title_us", function(result){		
				res.send({"search_id":result, "search_query":title,"case_data":case_data});
			});		
			
		}
	});
};



exports.esSearchTaxJudgement = function(req, res) {
	
	var judgement = req.body.postbody.judgement;

	client.search({
		index: 'tax_cases',
		type: 'tax-datas',
		size : 50,
		_source : ["case_id"],
		body: {
			query: {
				match: { "judgement": judgement }
			},
		}
	},function (error, response,status) {
		if(error){
			var case_ids = [1,2];
			res.send({"similar_cases" : case_ids}); 
		}
		else{
			if(req.body.username!=undefined){
				db.collection('my_account', function(err, collection) {
						  if(err)  {
							throw err;
						  }
						  else{
							var searchObject = {'search_query': judgement, timestamp: moment().format()};
							
							collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
								if(err){
									throw err;
								}
								else{
									console.log(item);
								}
							});
						  }
				});
			}
			var case_ids = [];
			var datas = response.hits.hits;
			query_result = [];
			datas.forEach(function(value){
				data = {};
				data["case_id"]  = value._source.case_id;
				query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
				case_ids.push(value._source.case_id);
			});
			
			cases.add_feedback(title, query_result, "essearch_judgement_tax", function(result){
				res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
			});
			
		}
	});
};



exports.findByTaxDateRange = function(req, res) {

	var date1 = req.query.start_date;
	var date2 = req.query.end_date;

	client.search({
		index: 'tax_cases',
		type: 'tax-datas',
		size : 50,
		_source : ["case_id"],
		body: {
			query: {
				filtered :{
					query: {
						match_all : { }
					},
					filter :{
						range : { "date" : { 'gte' : date1, 'lte' : date2}}
					}
				}
			},
		}
	},function (error, response,status) {
		if (error){
			console.log("search error: "+error);
			res.send({"response" : "unsuccessful"}) ;
		}
		else{
			
			var case_ids = [];
			var datas = response.hits.hits;
			query_result = [];
			datas.forEach(function(value){
				data = {};
				data["case_id"]  = value._source.case_id;
				query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
				case_ids.push(value._source.case_id);
			});
			
			cases.add_feedback(title, query_result, "essearch_year_tax", function(result){
				res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
			});
			
		}
	});
};

exports.esSearchTaxTitle = function(req, res) {
	
	var title = req.body.title;

	client.search({
		index: 'tax_cases',
		type: 'tax-datas',
		size : 50,
		_source : ["case_id"],
		body: {
			query: {
				match: { "case_title": title }
			},
		}
	},function (error, response,status) {
		if(error){
			var case_ids = [1];
			res.send({"similar_cases" : case_ids});
		}
		else{
			if(req.body.username!=undefined){
				db.collection('my_account', function(err, collection) {
						  if(err)  {
							throw err;
						  }
						  else{
							var searchObject = {'search_query': judgement, timestamp: moment().format()};
							console.log(searchObject);
							collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
								if(err){
									throw err;
								}
								else{
									console.log(item);
								}
							});
						  }
				});
			}
			var case_ids = [];
			var datas = response.hits.hits;
			query_result = [];
			datas.forEach(function(value){
				data = {};
				data["case_id"]  = value._source.case_id;
				query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
				case_ids.push(value._source.case_id);
			});
			
			cases.add_feedback(title, query_result, "essearch_year_tax", function(result){
				res.send({"search_id":result, "search_query":title,"similar_cases":case_ids});
			});
		}
	});
};

exports.findByCaseTitleES = function(req, res) {
	

	var title = req.body.postbody.title;
	
	client.search({
	  index: 'scr_cases_judis_new_merged_20170525',
	  type: 'scr-datas',
	  size : 50,
	  body: {
		query: {
			match : {
				"case_title": title
			}
		}
	  }
	},function (error, response,status) {
	if(error){
		var case_ids = [1];
		res.send({"similar_cases" : case_ids});
        
	}
	else{
		if(req.body.username!=undefined){
			db.collection('my_account', function(err, collection) {
					  if(err)  {
						throw err;
					  }
					  else{
						var searchObject = {'search_query': title, timestamp: moment().format()};
						console.log(searchObject);
						collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
							if(err){
								throw err;
							}
							else{
								console.log(item);
							}
						});
					  }
			});		
		}
		

		var case_ids = [];
		var case_data = [];
		var datas = response.hits.hits;
		console.log('here');
		console.log(datas);

		query_result = [];
		datas.forEach(function(value){								
				query_result.push({"case_id": value._source.case_id, "relevancy" : value._score });
				case_ids.push(value._source.case_id);
				case_data.push({"_source":value._source})
		});						
		
		cases.add_feedback(title, query_result, "essearch_scr_title", function(result){
			res.send({"search_id":result, "search_query":title,"case_data":case_data});
		});
	 }
 });
 
};

exports.findByCaseJudgement = function(req, res) {
	
	var judgement = req.body.postbody.judgement;
	console.time('test');
	client.search({
		index: 'scr_cases_judis_new_merged_20170525',
		type: 'scr-datas',
		size : 50,
		body: {
		query:{
				  match: { "judgement": judgement }
				  
				  /*match_phrase: {
					"judgement": {
						query: judgement,
						slop: 4
					}
				}*/
			/*bool:{
				  "should": {
					"match": { 
					  "judgement": {
						"query": judgement,
						"minimum_should_match": "30%"
					  }
					}
				  },
					"must": {
						"match_phrase": { 
						  "judgement": {
							"query": judgement,
							"slop":  100
						  }
						}
					  }
			}*/
		},
		highlight : {
				fields : {
					judgement : {
						pre_tags: ["<b class=\"mycolor\">"],
						post_tags: ["</b>"],
						"fragment_size": 150}
				}
		}
	}
	},function (error, response,status) {
		if(error){
			var case_ids = [1];
			res.send({"similar_cases" : case_ids});  
		}
		else{
			
			if(req.body.username!=undefined){
				db.collection('my_account', function(err, collection) {
					  if(err)  {
						throw err;
					  }
					  else{
						var searchObject = {'search_query': judgement, timestamp: moment().format()};
						
						collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
							if(err){
								throw err;
							}
							else{
								console.log(item);
							}
						});
					  }
				});	
			}	

			var case_ids = [];
			var case_data = [];
			var datas = response.hits.hits;
			console.log(datas);
			query_result = [];
			datas.forEach(function(value){
				case_data.push({"_source":value._source})
				query_result.push({"case_id": value._source.case_id, "relevancy" : value._score  } ) ;
			});
			
			cases.add_feedback(judgement, query_result, "essearch_scr_judgement", function(result){		
				console.timeEnd('test');
				res.send({"search_id":result, "search_query":judgement,"case_data":case_data});
			});		

		}
	});
	
};

exports.getSuggestions = function(req,res){
	console.log(req.body.text);
	client.suggest({
		index: 'suggestive_phrases_new_20171024',
		body: {
			docsuggest: {
				prefix: req.body.text,
				completion: {
					field: 'suggest',
					fuzzy: true
				}
			}
		}
	},function(error,response,status){
		if(error){
			res.send({"error in title ::: " : error });
		}
		else{
			phrase_arr = [];
			var datas = response.docsuggest[0].options;            
			
			datas.forEach(function(value){
				//phrase_arr.push(value.text)
				phrase_arr.push(value._source.name);
			});
			res.send({"phrases" : phrase_arr});
		}
	});
}
