var mongo = require('mongodb');
var assert = require('assert'); 

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

//var mongoHost = "52.77.250.60"; //staging
//var mongoHost = "52.77.59.43"; //production
var mongoHost = "127.0.0.1"; //local

var dbName = "pensieve";
//copy line
var collection_account = "account";

var server = new Server(mongoHost, 27017, {auto_reconnect: true});

db = new Db(dbName, server);

db.open(function(err, db) {
    if(!err) {
        console.log("Connected to 'pensieve' database");
        db.collection(collection_account, {strict:true}, function(err, collection) {
            if (err) {
                console.log("The 'pensieve' collection doesn't exist");
            }
        });

    }
});

//copy from here to cases

//Mailing Setup

var nodemailer = require('nodemailer');
var ses = require('nodemailer-ses-transport');

var transporter = nodemailer.createTransport(ses({
    accessKeyId: 'AKIAJAC6H33BKDZD7XWQ',
    secretAccessKey: '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'
}));
// Mailing setup ends

exports.signup = function(req, res) {
     var name = req.body['name'];
     var email = req.body['email'];
     var phone = req.body['phone'];
     var company = req.body['company'];
     var pass = req.body['newpassword'];
     var memtype = req.body['memtype'];
     var activated = 0;
     var access_token = "";
     var access_expiry = new Date();
     access_expiry.setDate(access_expiry.getDate()+15);
     var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
     for( var i=0; i < 20; i++ )
        access_token += possible.charAt(Math.floor(Math.random() * possible.length));
     
     var signup_json = /*JSON.stringify(*/{ 
        "name": name, 
        "email": email,
        "phone":phone,
        "company":company,
        "password":pass,
        "access type":memtype,
        "activated":activated,
        "access_expiry":access_expiry,
        "access_token":access_token
    };//);
    
     db.createCollection(collection_account, function(err, collection) {
            
            collection.findOne({'email': email}, function(err, item) {
                if (err) {
                    res.send("An Error Occured. Please try again later");
                    }
                    else if(item == null)
                    {
                        collection.insert(/*JSON.stringify(*/signup_json, function(err, result) {
                        if (err) {
                            console.log("error "+err);
                            res.send("An error occured. Please try again later");
                        } else {
                            var html = "Dear "+name+",<br/><br/>Please verify this email id by clicking this link:<br/><br/><a href='http://prototype.pensieve.co.in:7777/verifyregistration/"+email+"/"+access_token+"'>http://prototype.pensieve.co.in:7777/verifyregistration/"+email+"/"+access_token+"</a><br/><br/>Regards,<br/><br/>Team Pensieve<br/><br/>Advance Legal Search";
                            transporter.sendMail({
                                from: 'contact@pensieve.co.in',
                                to: email,
                                subject: 'Pensieve Account verification',
                                html: html
                              },function(err,status){
                                  if(err)
                                  {
                                      console.log("error while sending registration mail - " + err);
                                      res.send("An error occured. Please try again later");
                                  }
                                  else
                                      res.send("true");
                              });
                            
                        }
        
                        });
                    }
                    else
                    {
                        res.send("This email is already registered with us. Please Log In using that email");
                    }
            
            });
        //});
    });
}

// Adding By Raghuvar


exports.login = function(req,res)
{
    var username = req.body['username'];
    var pass = req.body['password'];
    console.log(username+","+pass);
    db.createCollection(collection_account, function(err, collection) {
        collection.findOne({'email': username,'password':pass}, function(err, item) {
            if(err)
            {
                res.send("An Error Occured. Please try again later");
            }
            else if(item == null)
            {
                res.send("Username or password is incorrect");
            }
            else if(item.activated == 0)
            {
                res.send("Please verify your account using the email sent to your registered email address");
            }
            else
            {
                res.send(item);
            }
        });
    });
}


exports.changepassword = function(req,res)
{
    console.log(req.body);
    var username = req.body['username'];
    var oldpass = req.body['oldpassword'];
    var pass = req.body['newpassword'];
    console.log(username+","+pass+","+oldpass);
    db.createCollection(collection_account, function(err, collection) {
        
        collection.findOne({'email':username,'password':oldpass},function(err,item){
            
            if(err)
            {
                res.send("An error occured. Please try again later");
            }
            else if(item == null)
            {
                res.send("The old password that you have entered is incorrect. Please try again");
            }
            else
            {
                collection.update({'email':username},{$set:{'password':pass}},function(err, result) {
            if(err)
            {
                res.send("An Error Occured. Please try again later");
            }
            else
            {
                res.send("true")
            }
        });
            }
            
        })
    });
}

exports.forgotpassword = function(req,res)
{
    var email = req.body['email'];
    console.log("Forgotten Password - "+email);
    db.createCollection(collection_account, function(err, collection) {
        collection.findOne({'email': email}, function(err, item) {
                if (err) {
                    res.send("An Error Occured. Please try again later");
                }
                else if(item == null)
                {
                    res.send("Sorry! We couldn't find your email registerd with us")
                }
                else
                {
                    var text = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                    for( var i=0; i < 10; i++ )
                        text += possible.charAt(Math.floor(Math.random() * possible.length));
                    collection.update({'email':email},{$set:{'password':text}},function(err, result) {
                        if (err) {
                            res.send("An Error Occured. Please try again later");
                        }
                        else{
                            var html = "Dear User,<br/><br/>Your new password is:<br/><br/>"+text+"<br/><br/> If you wish to set a password of your choosing, please do so by using the change password option after you log in<br/><br/>Regards,<br/><br/>Team Pensieve<br/><br/>Advance Legal Search";
                            transporter.sendMail({
                                from: 'contact@pensieve.co.in',
                                to: email,
                                subject: 'Pensieve Account verification',
                                html: html
                              },function(err,status){
                                  if(err)
                                  {
                                      console.log("error while sending registration mail - " + err);
                                      res.send("An error occured. Please try again later");
                                  }
                                  else
                                      res.send("true");
                              });
                        }
                    });

                }
        });
    });
}

exports.verifyregistration = function(req,res)
{
    var email = req.body['email'];
    var access_token = req.body['access_token'];
    console.log(access_token+","+email);
    db.createCollection(collection_account, function(err, collection) {
        collection.findOne({'email': email,'access_token':access_token}, function(err, item) {
            if(item == null)
            {
               res.send("Improper attempt to verify registration"); 
            }
            else
            {
            if(item.activated == 1)
            {
               res.send("Account is already active"); 
            }
            /*
            else if(new Date() > new Date(item.access_expiry))
            {
               res.send("The activation period has expired. Please contact the Pensieve team");
            }
            */
            else
            {
                collection.update({'email':email},{$set:{'activated':1}},function(err, result) {
                        if (err) {
                            res.send("An Error Occured. Please try again later");
                        }
                        else{
                            res.send("Congratulations ! Your 15 day free trial of Mitra has commenced. Let the Artificial Intelligence find the relevant cases while you focus on building your defensible argument");
                        }
            });
            }
            }
        });
    });
}

exports.dashboard = function(req,res){
   db.createCollection(collection_account, function(err, collection) {
        collection.findOne({'email': req.body.username}, function(err, user) {
		if(err){
		   res.json({'error':err});
		}
		else if(user.admin==1){
		  collection.find({}).toArray(function(err,allUsers){
			if(err){
			  res.send(err);
			}
			else{
			  res.send({'allUsers':allUsers});
			}
		  });
		}
		else{
		  res.json({'error':'You are not authorized to access this page!'});
		}
	   });
  });

}

exports.saveProspect = function(req,res){
   db.createCollection('prospects', function(err, collection) {
        collection.insert({'name': req.body.name,'email': req.body.email,'contact': req.body.contact,'company':req.body.company}, function(err, user) {
		if(err){
		   res.json({'error':err});
		}
		else{
		  res.send({'success':'Prospect added successfully!'});
		}
	   });
  });

}

exports.prospects = function(req,res){
   console.log(req.body.username);
   db.createCollection(collection_account, function(err, collection) {
        collection.findOne({'email': req.body.username}, function(err, user) {
		if(err){
		   res.json({'error':err});
		}
		else if(user.admin==1){
		  console.log(user);
		  db.createCollection('prospects', function(err, prospect_collection) {
			prospect_collection.find({}).toArray(function(err,allProspects){
				if(err){
				  res.send(err);
				}
				else{
				  res.send({'allProspects':allProspects});
				}
		  	});	
		  });
		  
		}
		else{
		  res.json({'error':'You are not authorized to access this page!'});
		}
	   });
  });

}