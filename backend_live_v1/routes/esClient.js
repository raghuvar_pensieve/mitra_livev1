const elasticsearch = require('elasticsearch');
const localhost = '127.0.0.1:9200'
const staging_host = '52.220.121.25:9200'
const esClient = new elasticsearch.Client({
    host: '127.0.0.1:9200',
    log: 'error'
  });

module.exports = esClient;
