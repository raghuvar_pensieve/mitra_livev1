var mongo = require('mongodb');
var assert = require('assert');
var request = require("request");
const util = require("util");
var subscribe = require("./Subscribe");
var moment = require("moment");
var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

//var mongoHost = "52.77.250.60"; //staging
var mongoHost = "52.77.59.43"; //production

// Creating a collection list to store all the email-ids of the subscriber
var dbName = "pensieve";
var collection_subscribe = "pensieve_subscriber_list";

var server = new Server(mongoHost, 27017, {auto_reconnect: true});

db = new Db(dbName, server); //test

db.open(function(err, db) {
    if(!err) {
        console.log("Connected to 'Subscriber' database");
        db.authenticate('pensieve_user', 'Pixel%0909', function(err, result) {
            assert.equal(true, result);
            console.log("database connection established");
            //db.close();
        });
        db.collection(collection_subscribe, {strict:true}, function(err, collection) {
            if (err) {
                console.log("The 'pensieve_subscriber_list' collection doesn't exist") ;
            }
        });

    }
});

// Function to save the subscriber in to the collection_subscribe collections in MongoDB
exports.saveSubscriber = function(req, res) {
    console.log("Subscriber List");
    var email = req.body.email;
    console.log('Retrieving EmailID:' + email);
    db.createCollection(collection_subscribe, function(err,collection){
    collection.findOne({'email': email}, function(err, item){
        if(err){
            console.log(err);
        }
        else if(item == null){
            collection.insert({'email': email}, function(err, result){
                if(err){
                    console.log(err);
                }
            });
        }
    });
});
};
