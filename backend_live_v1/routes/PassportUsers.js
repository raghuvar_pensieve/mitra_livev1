var mongo = require('mongodb');
var assert = require('assert');
var request = require("request");
const util = require("util");
var PassportUsers = require("./PassportUsers");
var moment = require("moment");
var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

var passport = require('passport');
var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;

//var mongoHost = "52.77.250.60"; //staging
var mongoHost = "52.77.59.43"; //production

// Creating a collection list to store all the email-ids of the subscriber
var dbName = "pensieve";
var collection_passportUser = "pensieve_passportUsers_list";

//pensieve_passportUsers_list is the name of the collection in which users entry will be stored.

var server = new Server(mongoHost, 27017, {auto_reconnect: true});

db = new Db(dbName, server); //test

db.open(function(err, db) {
    if(!err) {
        console.log("Connected to 'PassportUsers' database");
        db.authenticate('pensieve_user', 'Pixel%0909', function(err, result) {
            assert.equal(true, result);
            console.log("database connection established");
            //db.close();
        });
        db.collection(collection_passportUser, {strict:true}, function(err, collection) {
            if (err) {
                console.log("The 'pensieve_passportUsers_list' collection doesn't exist") ;
            }
        });

    }
});


// Function to save the passportGoogleUsers in to the collection_passportUsers_list collections in MongoDB
exports.google = function(req, res, done) {
        //console.log(req.body);
        var name = req.body['displayName'];
        var google_id = req.body['id'];
        var email = req.body['emails'][0].value;

        console.log(name);
        console.log(google_id);
        console.log(email);        
        //var name = req.body['name'];
        //var email = req.body['email'];
        var collection = collection_passportUser;
        //console.log(profile.displayName);
        //console.log(user);

        var insertElements = {
            "google_id" : google_id,
            "email" : email
        }   

//        function getUser(){
//        User.findById(id, function(err, user) {
//            done(err, user);
//        });

//                }

            db.createCollection(collection_passportUser, function(err,collection){
            collection.findOne({'google_id': google_id}, function(err, user){
                //console.log(user._id);
                if(err)
                    res.send("An error occurred");
    
                if(user){
                    console.log("user is already in the database" +user);
                    res.send(user);
                    // Already have that user
                }
                else if(user == null){
                        collection.insertOne(insertElements, function(err, result){
                            if(err){
                                console.log("Error " +err);
                            }
                            console.log("New User record created Successfully");
                            res.send(user);
                        });
                }
            });
        });

};


exports.getUser = function(req, res, done) {
        console.log(req);
        db.createCollection(collection_passportUser, function(err,collection){
                    collection.findOne({'_id' : req.body.id}, function(err, user){
                        if(err){
                            res.send("An error occurred");
                        }
                        else{
                            console.log(user);
                            res.send(user);
                        }
                    });
                });
};


// Function to save the passportLinkedinUsers in to the collection_passportUser collections in MongoDB
