var fs = require('fs');
var mongo = require('mongodb');
var constants = require('./constants')
var assert = require('assert');
const util = require('util')
var moment = require('moment');
var Server = mongo.Server,
	Db = mongo.Db,
	BSON = mongo.BSONPure;

//var mongoHost = "52.77.250.60"; //staging
//production  = '52.77.59.43'
var mongoHost = "52.77.59.43"; //production

var dbName = "pensieve";
var collection = "scr_cases_judis_20171012";
var collection_corpus_id = "scr_case_corpus_index_new"
var collection_feedback = "feedback"
var collection_account = "account";


/*var server = new Server(mongoHost, 27017, {auto_reconnect: true});
db = new Db(dbName, server); //test

db.open(function(err, db) {
	if(!err) {
		console.log("Connected to 'cases' database");
		db.collection(collection, {strict:true}, function(err, collection) {
			if (err) {
				console.log("The 'cases' collection doesn't exist");
			}
		});
	}
});*/

var server = new Server(mongoHost, 27017, {auto_reconnect: true});

db = new Db(dbName, server); //test
db.open(function(err, db) {
	
	if(!err) {
		console.log("Connected to 'cases' database");
		db.authenticate('pensieve_user', 'Pixel%0909', function(err, result) {
			assert.equal(true, result);

			//db.close();
		});
		db.collection(collection, {strict:true}, function(err, collection) {
			if (err) {
				console.log("The 'cases' collection doesn't exist");
			}
		});

	}
});
//-----------------Login-signup code---------------------------------
var nodemailer = require('nodemailer');
var ses = require('nodemailer-ses-transport');

var transporter = nodemailer.createTransport(ses({
	accessKeyId: 'AKIAJAC6H33BKDZD7XWQ',
	secretAccessKey: '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'
}));
// Mailing setup ends

exports.signup = function(req, res) {
	console.log(req.body);
	if(req.body.accesscode == "Mitra30"){

		var name = req.body['name'];
		var email = req.body['email'];
		var phone = req.body['phone'];
		var company = req.body['company'];
		var pass = req.body['newpassword'];
		var memtype = '1';//req.body['memtype'];
		var activated = 1;
		var verified = 0;
		var access_token = "";
		var access_expiry = new Date();
		var activation_date = new Date();
		access_expiry.setDate(access_expiry.getDate()+30);
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for( var i=0; i < 20; i++ )
		access_token += possible.charAt(Math.floor(Math.random() * possible.length));

		var signup_json = /*JSON.stringify(*/{
		"name": name,
		"email": email,
		"phone":phone,
		"company":company,
		"password":pass,
		"access type":memtype,
		"verified": verified,
		"activated":activated,
		"access_expiry":access_expiry,
		"activation_date":activation_date,
		"access_token":access_token
		};//);

		var searchedQueries = [];
		var savedCases = [];

		var my_account = {
			'email': email,
			'searchedQueries': searchedQueries,
			'savedCases': savedCases
		};

		db.createCollection(collection_account, function(err, collection) {

			collection.findOne({'email': email}, function(err, item) {
				if (err) {
					res.send("An Error Occured. Please try again later");
					}
					else if(item == null)
					{
						collection.insert(/*JSON.stringify(*/signup_json, function(err, result) {
						if (err) {
							console.log("error "+err);
							res.send("An error occured. Please try again later");
						} else {
							var html = "Hi "+name +"," +
							"<br/>" +
							"<br/>"+
							"Thank you for registering with Mitra."+
							"<br>Mitra is an Artificial Intelligence Driven Legal Research Platform."+
							"<br>"+
							"<br>Mitra's Artificial Intelligence Engine looks into context and associations to help legal researcher get new insights from legal information."+
							"<br> To start using our Legal intelligence platform, please verify your email-id by clicking this link:"+
							"<br/>"+
							"<br/><a href='http://prototype.pensieve.co.in:7777/verifyregistration/"+email+"/"+access_token+"'>http://prototype.pensieve.co.in:7777/verifyregistration/"+email+"/"+access_token+"</a>"+
							"<br/>"+
							"<br/>Regards,"+
							"<br/><b>Mitra</b>"+
							"<br/>Artificial Intelligence Driven Legal Research Platform";
							transporter.sendMail({
								from: 'contact@pensieve.co.in',
								to: email,
								subject: 'Welcome to Mitra',
								html: html
							  },function(err,status){
								  if(err)
								  {
									  console.log("error while sending registration mail - " + err);
									  res.send("An error occured. Please try again later");
								  }
								  else
									  res.send("true");
							  });

						}

						});
					}
					else
					{
						res.send("This email is already registered with us. Please Log In using that email");
					}

			});
		//});
		});

		db.createCollection('my_account',function(err,collection){

			collection.findOne({'email': email}, function(err, item) {
						if (err) {
							console.log(err);
						}
						else if(item == null)
						{
							collection.insert(/*JSON.stringify(*/my_account, function(err, result) {
								if (err) {
									console.log("error "+err); 
								}
							});
						}            
			});
		});


	}
	else{
		res.send("The access code is not valid!");
	}
	 


}

exports.login = function(req,res)
{
	console.log('here');
	var username = req.body['username'];
	var pass = req.body['password'];
	var searchedQueries = [];
	var savedCases = [];
	var my_account = {
		'email': username,
		'searchedQueries': searchedQueries,
		'savedCases': savedCases
	};
	console.log(username+","+pass);
	db.createCollection(collection_account, function(err, collection) {
		collection.findOne({'email': username,'password':pass}, function(err, item) {
			if(err)
			{
				res.send("An Error Occured. Please try again later");
			}
			else if(item == null)
			{
				res.send("Username or password is incorrect");
			}
			else if(item.activated == 0)
			{
				res.send("Your subscription period is over!");
			}
			else if(item.verified == 0)
			{
				res.send("Please verify your account using the email sent to your registered email address");
			}
			else
			{
				console.log(item);
				res.send(item);
			}
		});
	});

	db.createCollection('my_account', function(err, collection) {
		collection.findOne({'email': username}, function(err, item) {
			if(err)
			{
				console.log("An Error Occured. Please try again later");
			}
			else if(item == null)
			{
				collection.insert(my_account, function(err, result) {
						if (err) {
							console.log("error "+err);
							
						} else {
							
							console.log(result);
						}
		
				});
			}
			else
			{
				//console.log(item);
			}
		});
	});
}


exports.changepassword = function(req,res)
{
	console.log(req.body);
	var username = req.body['username'];
	var oldpass = req.body['oldpassword'];
	var pass = req.body['newpassword'];
	console.log(username+","+pass+","+oldpass);
	db.createCollection(collection_account, function(err, collection) {

		collection.findOne({'email':username,'password':oldpass},function(err,item){

			if(err)
			{
				res.send("An error occured. Please try again later");
			}
			else if(item == null)
			{
				res.send("The old password that you have entered is incorrect. Please try again");
			}
			else
			{
				collection.update({'email':username},{$set:{'password':pass}},function(err, result) {
			if(err)
			{
				res.send("An Error Occured. Please try again later");
			}
			else
			{
				res.send("true")
			}
		});
			}

		})
	});
}

exports.forgotpassword = function(req,res)
{
	var email = req.body['email'];
	console.log("Forgotten Password - "+email);
	db.createCollection(collection_account, function(err, collection) {
		collection.findOne({'email': email}, function(err, item) {
				if (err) {
					res.send("An Error Occured. Please try again later");
				}
				else if(item == null)
				{
					res.send("Sorry! We couldn't find your email registerd with us")
				}
				else
				{
					var text = "";
					var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
					for( var i=0; i < 10; i++ )
						text += possible.charAt(Math.floor(Math.random() * possible.length));
					collection.update({'email':email},{$set:{'password':text}},function(err, result) {
						if (err) {
							res.send("An Error Occured. Please try again later");
						}
						else{
							var html = "Dear User,<br/><br/>Your new password is:<br/><br/>"+text+"<br/><br/> If you wish to set a password of your choosing, please do so by using the change password option after you log in<br/><br/>Regards,<br/><br/>Team Pensieve<br/><br/>Advance Legal Search";
							transporter.sendMail({
								from: 'contact@pensieve.co.in',
								to: email,
								subject: 'Pensieve Account verification',
								html: html
							  },function(err,status){
								  if(err)
								  {
									  console.log("error while sending registration mail - " + err);
									  res.send("An error occured. Please try again later");
								  }
								  else
									  res.send("true");
							  });
						}
					});

				}
		});
	});
}

exports.verifyregistration = function(req,res)
{
	var email = req.body['email'];
	var access_token = req.body['access_token'];
	console.log(access_token+","+email);
	db.createCollection(collection_account, function(err, collection) {
		collection.findOne({'email': email,'access_token':access_token}, function(err, item) {
			if(item == null)
			{
			   res.send("Improper attempt to verify registration");
			}
			else
			{
			if(item.verified == 1)
			{
			   res.send("Account is already active");
			}
			/*
			else if(new Date() > new Date(item.access_expiry))
			{
			   res.send("The activation period has expired. Please contact the Pensieve team");
			}
			*/
			else
			{
				collection.update({'email':email},{$set:{'verified':1, 'activated':1}},function(err, result) {
						if (err) {
							res.send("An Error Occured. Please try again later");
						}
						else{
							res.send("Congratulations ! Your 15 day free trial of Mitra has commenced. Let the Artificial Intelligence find the relevant cases while you focus on building your defensible argument");
						}
			});
			}
			}
		});
	});
}

exports.myaccount = function(req,res)
{
	var username = req.body['username'];
	
	db.createCollection('my_account', function(err, collection) {
		collection.findOne({'email': username}, function(err, item) {
			if(err)
			{
			   res.json({'error':err});
			}            
			else
			{   
				res.json({'userData':item});
			}
		});
	});
}





//-----------------Login-signup ends-----------------------------------


var request = require('request');
exports.findSimilarByCaseId = function(req, res) {
	var case_id = req.body['case_id'];
	console.log("body "+ case_id)
	//var dbh = db;
	request.post(
		'http://52.77.161.203:3000/similarCasesByCaseId',
		{ json: { 'case_id': case_id } },
		function (error, response, body) {

			if (!error && response.statusCode == 200) {
				var corpus_ids =[];
				var relevance_array= [];
				var corpus_dict = {};
				console.log(body);
				for (i=0; i<body['case'].length; i++) {
					corpus_ids.push(body['case'][i][0]);
					relevance_array.push(body['case'][i][1]);
					corpus_dict[body['case'][i][0]] = body['case'][i][1];
					// here jsonObject['sync_contact_list'][i] is your current "bit"
				}
				if(corpus_ids.length>0){
					db.collection(collection, function(err, collection) {
						if (err) {
							throw err;
						} else{
							collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item){

								
								for(j=0; j<item.length;j++){
									//item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
									if(constants.DUPLICATES_SCR.indexOf(item[j]['case_id']) > 0){
											item.splice(j, 1);
											j--;
									}else{
											item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
									}
								}
								console.log('total cases ' + item.length)

								//it.set('relevancy', corpus_dict[item[0]['corpus_id']] );
								item.sort(function(a, b) {
									return parseFloat(b.relevancy) - parseFloat(a.relevancy);
								});
								res.send({"request_case":case_id,"similar_cases":item});
							});
						}
					});
				}else {
					console.log("error1: ");
					console.log(corpus_ids);
					res.send({"error":"Error from server"});
				}
			}else{
				console.log(error);
				res.send({"error":"Error from server"});
			}
		});
}

// Finds similar cases from input Text
 exports.findSimilarByText = function(req, res) {
	console.log(req.body);
	var search_query = req.body.postbody['search_query'];
	console.log("body "+ search_query)
	request.post(
		'http://52.77.161.203:3000/similarCasesByText',
		{ json: { 'search_query': search_query } },
		function (error, response, body) {
			if (!error && response.statusCode == 200) {
				console.log(util.inspect(body, false, null));
				var corpus_ids =[];
				var relevance_array= [];
				var corpus_dict = {};
				for (i=0; i<body['case'].length; i++) {
					corpus_ids.push(body['case'][i][0]);
					relevance_array.push(body['case'][i][1]);
					corpus_dict[body['case'][i][0]] = body['case'][i][1];
					// here jsonObject['sync_contact_list'][i] is your current "bit"
				}
				
				if(corpus_ids.length>0){
					db.collection(collection_corpus_id, function(err, collection) {
						if (err) {
							throw err;
						} else {
							collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item) {
								if(err){
									throw err;
								}else{
									var idsProjects = [];
									for(j=0; j<item.length;j++){
										if(constants.DUPLICATES_SCR.indexOf(item[j]['case_id']) > 0){
											item.splice(j, 1);
											j--;
										}else{
											item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
										}
									}
									item.sort(function(a, b) {
										return parseFloat(b.relevancy) - parseFloat(a.relevancy);
									});
									for(j=0;j<item.length;j++){
										idsProjects.push(item[j].case_id);
									}
									add_feedback(search_query, item, "lda_model", function(result){

										db.collection("scr_cases_judis_merged_20170525", function(err, collection) {
											if (err) {
												throw err;
												console.log("error");
												res.send({"Error":"Error occurred on server"});
											} else {
												collection.find({'case_id': {$in:idsProjects}}).toArray(function(err, docs){												
													docs.sort(function(a, b) {
														// Sort docs by the order of their _id values in ids.
														return idsProjects.indexOf(a.case_id) - idsProjects.indexOf(b.case_id);
													});	
													res.send({"search_id":result, "search_query":search_query,"similar_cases":item,case_data:docs})
												});
												
											}
										});
										
									});
								}
							});
						}
					});

					db.collection('my_account', function(err, collection) {
					  if(err)  {
						throw err;
					  }
					  else{
						var searchObject = {'search_query': search_query, timestamp: moment().format()};                        
						collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
							if(err){
								throw err;
							}
							else{
								console.log(item);
							}
						});
					  }
					});
					
				} else{
					res.send({"error":"No similar results found"});
				}
			} else{
				res.send({"error":response});
			}
	});
	

	
}



// Finds case data from DB for multiple case_ids
exports.findByCaseIds = function(req, res) {
	var id = req.body;
	console.log(req.body);
	console.log('Retrieving case: ' + id.cases);
	var idsProjects  = id['cases'];
	//var b = idsProjects.split(',').map(Number);
	db.collection(collection, function(err, collection) {

		if (err) {
			throw err;
			console.log("error");
			res.send({"Error":"Error occurred on server"});
		} else {
			collection.find({'case_id': {$in:idsProjects}}).toArray(function(err, docs){ //, function(err, item) {
			//collection.findOne({'case_id': Number(id), function(err, item) {
				docs.sort(function(a, b) {
					// Sort docs by the order of their _id values in ids.
					return idsProjects.indexOf(a.case_id) - idsProjects.indexOf(b.case_id);
				});
				
				res.send(docs);
				docs.forEach(function(doc){
					console.log(doc.case_id);
				});
			});
			
		}
	});
};


// Feedback APIs start here, add feedback object
exports.add_feedback = function(search_query, search_result, search_with, callback) {

	var feedback_json = /*JSON.stringify(*/{
		"search_query": search_query,
		"search_result": search_result,
		"search_with" : search_with
	};//);
	//console.log('Adding feedback: ' + JSON.stringify(feedback_json));
	db.createCollection(collection_feedback, function(err, collection) {
			collection.insert(/*JSON.stringify(*/feedback_json, function(err, result) {
				if (err) {
					console.log("error "+err);
					callback ('');
			return feedback_json._id;
				} else {
					console.log("feedback_id" +feedback_json._id);
					callback(feedback_json._id);
					return feedback_json._id;
				}
			});
		//});
	});
};


// Feedback API, search results's feedback
exports.search_result_feedback = function(req, res) {
	var search_id = req.body['search_id'];
	var rating = req.body['rating'];
	var feedback = req.body['feedback'];

	console.log('Updating feedback: ' + search_id + " " + " "+ feedback);
	//db.collection(collection_feedback, function(err, collection) {
	db.collection(collection_feedback).update({'_id':new mongo.ObjectID(search_id)}, {$set:{'result_rating':rating, 'result_feedback':feedback}}, function(err, result) {
		if (err) {
			console.log('Error updating result feedback: ' + err);
			res.send({'error':'Could not update feedback'});
		} else {
			console.log('' + result + 'Feedback submitted');
			res.send(JSON.stringify({"search_id":search_id, "message":"feedback received successfully"}));
		}
	});
	//});
}

// feedback for each case
exports.case_feedback = function(req, res) {
	var search_id = req.body['search_id'];
	var case_id = req.body['case_id'];
	var rating = req.body['rating'];
	var case_data = {"case_id":case_id,"rating":rating};

	console.log('Updating feedback: ' + search_id + " " + " "+ case_id);
	//db.collection(collection_feedback, function(err, collection) {
	db.collection(collection_feedback).update({'_id':new mongo.ObjectID(search_id)}, {$push: { 'cases': case_data }}, function(err, result) {
		if (err) {
			console.log('Error updating result feedback: ' + err);
			res.send({'error':'Could not update feedback'});
		} else {
			console.log('' + result + 'Feedback submitted');
			res.send(JSON.stringify({"search_id":search_id, "case_id":case_id, "message":"feedback received successfully"}));
		}
	});
}

exports.similarTextWordcloud = function(req, res) {
	var search_query = req.body['search_query'];
	request.post(
		'http://52.77.161.203:3000/similar_by_text_wordcloud',
		{ json: { 'search_query': search_query } },
		function (error, response, body) {
			if (!error && response.statusCode == 200) {
				var filters=response.body.word_cloud;
				var similar_cases=response.body.similar_cases;

				var lenCorpus=similar_cases.length;
				var filters_corpusids={};
				var corpus_ids=[];

				for(i=0;i<lenCorpus;i++) {
					var filter_topics=similar_cases[i].words_from_topic;
					var lentopic=filter_topics.length;
					for(j=0;j<lentopic;j++) {
						if(filters_corpusids[filter_topics[j]]==undefined) {
							filters_corpusids[filter_topics[j]]=[];    
						}
						filters_corpusids[filter_topics[j]].push(similar_cases[i].corpus_id);
					}
				}

				//Convert corpus to case
				var objCases={};
				var lenObj=Object.keys(filters_corpusids).length;

				db.collection(collection_corpus_id, function(err, collection) {
					if (err) {
						throw err;
					} else{
						for(filter_data in filters_corpusids) {
							var corpus_ids=filters_corpusids[filter_data];
							if(corpus_ids.length>0) {
								filtercallback(collection,corpus_ids,objCases,filter_data, function(callback) {
									var lencallback=Object.keys(callback).length;
									if(lencallback==lenObj) {
										res.send({'filters':filters,'filter_cases':callback});
										//console.log(callback);
									}
								});
							}
						}
					}
				});
			}
			else{
				res.send({"error":error});
			}
		}
	);
}
filtercallback=function(collection,corpus_ids,objCases,filter_data,callback) {
	console.log('params');
	collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item) {
		if (err) {
			throw err;
		} else {
			if(objCases[filter_data]==undefined) {
				objCases[filter_data]=[];
			}
			var itemlen=item.length;
			for(j=0; j<itemlen;j++){
				objCases[filter_data].push(item[j]['case_id']);
			}
			callback(objCases);
		}
	});    
}

exports.displayFilteredData=function(req,res) {
	var search_query = req.body['search_query'];
	var filters_query = req.body['filters'];
	var arrFilters = filters_query.split("&");
	var lenFilters=arrFilters.length;
	var corpus_dict = {};
	request.post(
		'http://52.77.161.203:3000/similar_by_text_wordcloud',
		{ json: { 'search_query': search_query } },
		function (error, response, body) {
			//console.log('response==='+JSON.stringify(response));
			if (!error && response.statusCode == 200) {
				//console.log('response=='+JSON.stringify(response));	
				var corpus_ids=[];
				var similar_cases = response.body.similar_cases;
				var lenSimilarCases = similar_cases.length;
				//console.log('response=='+JSON.stringify(similar_cases));	
				for(i=0;i<lenSimilarCases;i++) {
					var add=1;
					var topics=similar_cases[i].words_from_topic;
					for(j=0;j<lenFilters;j++) {
						if(arrFilters[j].indexOf(topics)!=-1) {
							add=0;
							break;
						}
					}
					if(add==1) {
						corpus_ids.push(similar_cases[i].corpus_id);
					}
				}
				console.log(corpus_ids);
				
				if(corpus_ids.length>0){
					db.collection(collection_corpus_id, function(err, collection) {
						if (err) {
							throw err;
						} else{
							collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item){ //, function(err, item) {
								if (err) {
									throw err;
								} else {
									//console.log(corpus_dict[corpus_ids[0]   ]);
									for(j=0; j<item.length;j++){
										item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
									}
									//it.set('relevancy', corpus_dict[item[0]['corpus_id']] );
									item.sort(function(a, b) {
										return parseFloat(b.relevancy) - parseFloat(a.relevancy);
									});
									var search_id;
									
									var feedback_json = { 
											"search_query": search_query, 
											"search_result": item
									};
									collection.insert(feedback_json, function(err, result) {
										if (err) {
											console.log("error "+err);
											res.send({"search_id":search_id,"search_query":search_query,"similar_cases":item});
											//callback ({'error':'Error in add_feedback'});
										} else {
											console.log('Success: ' + feedback_json._id);
											//callback(feedback_json._id);
											search_id = feedback_json._id;
											res.send({"search_id":search_id,"search_query":search_query,"similar_cases":item});
											//return feedback_json._id
										}
									});
								}
								//res.send({"search_id":search_id,"search_query":search_query,"similar_cases":item});
							});
						}
					});
				} else{
					res.send({"error":"No similar results found"});
				}
			}
		});
}


exports.saveNote = function(req,res){
	console.log(req.body);
	db.collection('my_account', function(err, collection) {
	  if(err)  {
		throw err;
	  }
	  else{
	  	if(req.body.pageName=='loadNote'){
	  		collection.update({$and:[{'email':req.body.email},{'savedNotes.tag':req.body.tag}]},{$set:{"savedNotes.$.content":req.body.content}},function(err,item){
	  			if(err){
					res.json({"message":"error"});
				}
				else{
					console.log(item);
					res.json({"message":"successful"});
				}
	  		})
	  	}
	  	else{
	  		var note = {'content': req.body.content, 'pageName': req.body.pageName, 'tag': req.body.tag, timestamp: moment().format()};
			collection.findOneAndUpdate({'email': req.body.email},{$push:{savedNotes:note}}, function(err, item){
				if(err){
					res.json({"message":"error"});
				}
				else{
					console.log(item);
					res.json({"message":"successful"});
				}
			});	
	  	}
	  }
	});
}

exports.loadNote = function(req,res){
	
	db.collection('my_account', function(err, collection) {
	  if(err)  {
		throw err;
	  }
	  else{
		console.log(req.params);
		collection.aggregate([{$unwind:"$savedNotes"},{$match:{$and:[{'email':req.body.email},{"savedNotes.tag":req.params.tag}]}},{$project:{savedNotes:1}}]).toArray(function(err,item){
			if(err){
				res.json({"message":"error"});
			}
			else{
				console.log(item);
				res.json({"content":item[0].savedNotes.content});
			}
		});
		/*collection.find({$and:[{'email':req.body.email},{'savedNotes.tag':req.params.tag}]},{savedNotes.tag:1}).toArray(function(err, item){
			if(err){
				res.json({"message":"error"});
			}
			else{
				console.log(item);
				res.json({"content":item});
			}
		});*/
	  }
	});
}





//This is testing code from here -----------------------------------------------------------

// Finds one case for given case_id
exports.findByCaseId = function(req, res) {
	var id = req.params.id;
	console.log('Retrieving case: ' + id);

	db.collection(collection, function(err, collection) {

		if (err) {
			throw err;
			console.log("error");
		} else {
				collection.findOne({'case_id': Number(id)}, function(err, item) {

				res.send(item);
			});
		}
	});
};

var request = require('request');
exports.findSimilarCasedataByCaseId = function(req, res) {
	var case_id = req.body['case_id'];
	console.log("body "+ case_id)
	//var dbh = db;
	request.post(
		'http://52.77.161.203:3000/similarCasesByCaseId',
		{ json: { 'case_id': case_id } },
		function (error, response, body) {

			if (!error && response.statusCode == 200) {
				var corpus_ids =[];
				var relevance_array= [];
				var corpus_dict = {};
				for (i=0; i<body['case'].length; i++) {
					corpus_ids.push(body['case'][i][0]);
					relevance_array.push(body['case'][i][1]);
					corpus_dict[body['case'][i][0]] = body['case'][i][1];
					// here jsonObject['sync_contact_list'][i] is your current "bit"
				}
				console.log(corpus_ids);
				if(corpus_ids.length>0){
					db.collection(collection_corpus_id, function(err, collection) {
						if (err) {
							throw err;
						} else{
							var cases = [];
							collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item){ //, function(err, item) {
							//console.log(corpus_dict[corpus_ids[0]   ]);
							for(j=0; j<item.length;j++){
								item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
								cases.push(item[j]['case_id'])
							}
							//it.set('relevancy', corpus_dict[item[0]['corpus_id']] );
							item.sort(function(a, b) {
								return parseFloat(b.relevancy) - parseFloat(a.relevancy);
							});
							for(k=0;k<item.length;k++){
								var cases = item['case_id'];
							}
							res.send({"request_case":case_id,"similar_cases":item});
							});
						}
					});
				}else {
					res.send({"error":"Error from server"});
				}
			}else{
				res.send({"error":"Error from server"});
			}
		});
}







exports.deleteFeedback = function(req, res) {
	var id = req.params.id;
	console.log('Deleting wine: ' + id);
	db.collection('wines', function(err, collection) {
		collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
			if (err) {
				res.send({'error':'An error has occurred - ' + err});
			} else {
				console.log('' + result + ' document(s) deleted');
				res.send(req.body);
			}
		});
	});
}

// Get all cases from DB
exports.findAll = function(req, res) {
	db.collection(collection, function(err, collection) {
		collection.find().toArray(function(err, items) {
			res.send(items);
		});
	});
};




exports.findLast = function(req, res) {
	db.collection(collection, function(err, collection) {
		collection.find().limit(1).toArray(function(err, items) { //http://localhost:3000/findLast
			res.send(items);
		});
	});
};

exports.saveCaseId = function(req,res){
	console.log(req.body.data_type);
	var collection_name = '';
	if(req.body.data_type=='us'){
		
		db.collection('us_data',function(err,cases){
			cases.findOne({'case_id_source':Number(req.body.caseId)},function(err,caseDetails){
				if(err){
					console.log(err);
				}
				db.collection('my_account', function(err, collection) {
				  if(err)  {
					console.log(err);
				  }
				  else{
					var caseObject = {'title': caseDetails.case_title,'case_id':req.body.caseId,'type':'us', timestamp: moment().format()};
					collection.findOneAndUpdate({'email': req.body.username},{$push:{savedCases:caseObject}}, function(err, item){
						if(err){
							console.log(err);
						}
						else{
							res.json({"message":"successful"});
							//console.log(item);
						}
					});
				  }
				});        
			});
		});
	}
	else{
		db.collection(collection,function(err,cases){
			cases.findOne({'case_id':Number(req.body.caseId)},function(err,caseDetails){
				if(err){
					console.log(err);
				}
				db.collection('my_account', function(err, collection) {
				  if(err)  {
					console.log(err);
				  }
				  else{
					var caseObject = {'title': caseDetails.case_title,'case_id':req.body.caseId,'type':'scr',timestamp: moment().format()};
					collection.findOneAndUpdate({'email': req.body.username},{$push:{savedCases:caseObject}}, function(err, item){
						if(err){
							console.log(err);
						}
						else{
							res.json({"message":"successful"});
							//console.log(item);
						}
					});
				  }
				});        
			});
		});
	}
}
var add_feedback = exports.add_feedback
