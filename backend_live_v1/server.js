var express = require('express');
var cors = require('cors');
var cases = require('./routes/cases');
var cluster = require('cluster');
var numCPUs = require('os').cpus().length;
var account = require('./routes/account');
var compression = require('compression');
var app = express();
var esSearch = require('./routes/elasticSearchRequest');
var PassportUsers = require('./routes/PassportUsers');

//var passport    = require('passport');

var bodyParser = require("body-parser");
var subscribe = require('./routes/Subscribe');
// subscribe is the API which we are going to declare in the routes
// app.configure(function () {
//     app.use(express.logger('dev'));      'default', 'short', 'tiny', 'dev'
//     app.use(express.bodyParser());
// 	//app.use(bodyParser.urlencoded({extended: true}));

// });
//app.use(express.logger('dev'));     /* 'default', 'short', 'tiny', 'dev' */

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(compression());
// app.configure(function () {
//     app.use(express.logger('dev'));      'default', 'short', 'tiny', 'dev'
//     app.use(express.bodyParser());
// 	//app.use(bodyParser.urlencoded({extended: true}));
//added by raghuvar(Just below 2 lines)
//app.use(passport.initialize());
//app.use(passport.session());


app.post('/signup',cases.signup);
app.post('/login',cases.login);
app.post('/changepassword',cases.changepassword);
app.post('/forgotpassword',cases.forgotpassword);
app.post('/verifyregistration',cases.verifyregistration);
app.post('/myaccount',cases.myaccount);
app.post('/saveNote', cases.saveNote);
app.post('/loadNote/:tag', cases.loadNote);
app.post('/dashboard',account.dashboard);
app.post('/dashboard/saveProspect',account.saveProspect);
app.post('/dashboard/prospects',account.prospects);
app.post('/verify', subscribe.saveSubscriber);

// Verify is the API for the subscriber list
// });
//var passportfront = require('./routes/passportfront');
//var oauth = require('./routes/oauth');

// Adding the passport functionality (Raghuvar)
//app.post('/googlelogin', passport.googlelogin);
//app.post('/auth/google/callback', passport.callback);
app.post('/google', PassportUsers.google);
app.post('/google/getUser', PassportUsers.getUser);
//app.post('/linkedin', PassportUsers.linkedin);


TaxCases = require('./routes/TaxCases');
app.post('/similarByTaxText', TaxCases.findSimilarByText);
app.post('/casesby_tax_ids', TaxCases.findCasesByTaxIds);
app.post('/tax_similarbycaseid', TaxCases.findSimilarByTaxId);
// caseby_tax_ids
// caseby_tax_ids
var esSearch = require('./routes/elasticSearchRequest');
app.post('/esSearchTaxTitle', esSearch.esSearchTaxTitle);
app.post('/es_tax_by_date', esSearch.findByTaxDateRange);
app.post('/es_tax_by_judgement', esSearch.esSearchTaxJudgement);

app.post('/esSearchDelhiTitle', esSearch.esSearchDelhiTitle);
app.post('/esSearchDelhiJudgement', esSearch.esSearchDelhiJudgement);

app.post('/esSearchMumbaiTitle', esSearch.esSearchMumbaiTitle);
app.post('/esSearchMumbaiJudgement', esSearch.esSearchMumbaiJudgement);

app.post('/esSearchUSTitle', esSearch.esSearchUSTitle);
app.post('/esSearchUSJudgement', esSearch.esSearchUSJudgement);

app.post('/esSearch', esSearch.findByCaseTitleES);
app.post('/essearch_by_judgement', esSearch.findByCaseJudgement);
app.post('/suggestive_phrases',esSearch.getSuggestions);

app.get('/cases', cases.findAll);
app.get('/cases/:id', cases.findByCaseId);
app.post('/casesbyids', cases.findByCaseIds);
app.get('/findlast', cases.findLast);
app.post('/similarbycaseid', cases.findSimilarByCaseId);
app.post('/similarbytext', cases.findSimilarByText);
app.post('/similartextwordcloud', cases.similarTextWordcloud);
app.post('/displayfiltereddata', cases.displayFilteredData);
app.post('/saveCaseId',cases.saveCaseId);

app.post('/search_result_feedback', cases.search_result_feedback);
app.post('/case_feedback', cases.case_feedback);

Summariser = require('./routes/summariser');
app.post('/summarise', Summariser.summarise);

mum_cases = require('./routes/MumbaiCases');
app.post('/mum_similarbycaseid', mum_cases.findSimilarByCaseId);
app.post('/mum_similarbytext', mum_cases.findSimilarByText);
app.post('/casesby_mumbai_ids', mum_cases.findCasesByCaseIds);

delhi_cases = require('./routes/DelhiCases');
app.post('/delhi_similarbycaseid', delhi_cases.findSimilarByCaseId);
app.post('/delhi_similarbytext', delhi_cases.findSimilarByText);
app.post('/casesby_delhi_ids', delhi_cases.findCasesByCaseIds);

US_cases = require('./routes/USCases');
app.post('/us_similarbycaseid', US_cases.findSimilarByCaseId);
app.post('/us_similarbytext', US_cases.findSimilarByText);
app.post('/cases_by_us_ids', US_cases.findCasesByCaseIds);
app.post('/similarustextwordcloud', US_cases.similarUSTextWordcloud);


if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);

  // Fork workers.
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} 
else {
  app.listen(3000);
  console.log('Listening on port 3000...');
}
