import logging
#logging.basicConfig(filename='./logfile.log', format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

#import stuff
import gensim
from gensim import *
import re, os, string
import numpy as np
from time import time
from numpy import array

root = "/home/ec2-user/taxman_test/"

tax_corpus_file_address = root + "taxmann_corpus.mm"
tax_dictionary_file_address = root + "taxmann_dict.dict"
tax_lda_model_file_address  = root + "taxman_lda_model_19400_cases_75_topics_100_passes_500_iterations.model"
tax_index_file_address = root + "taxman_index_19400_cases_75_topics_100_passes_500_iterations.index"

# Load corpora and dictionary
# sercorpus = corpora.MmCorpus('../../workspace/recovered_corpus_20161102_ser28kcorpus.mm')
sercorpus = corpora.MmCorpus(tax_corpus_file_address)
# dictionary= corpora.Dictionary.load('../../workspace/recovered_dictionary_20161102.dict')
dictionary= corpora.Dictionary.load(tax_dictionary_file_address)
# print(dictionary)

# Load Similarity
lda_model = gensim.models.ldamodel.LdaModel.load(tax_lda_model_file_address)
# lda_model = gensim.models.ldamodel.LdaModel.load('../../workspace/REcovered_20161102_LDA_Model_Multicore_100_Passes_Serialized_corpus_100_topics.model')
index = similarities.Similarity.load(tax_index_file_address)
# index = similarities.Similarity.load('../../workspace/Recovered_similarity_index_20161102.index')

def find_similar_tax_doc( doc):
    #Search Query
    #doc = "bail in road rage"
    #file = open('data/Case-32.txt','r')
    #doc = file.read()
    vec_bow = dictionary.doc2bow(doc.lower().split())
    #print vec_bow

    #Search_vectors
    vec_lda = lda_model[vec_bow]
    #vec_HDP = model_HDP[vec_bow]
    #print(vec_lda)
    #print(vec_HDP)

    index.num_best=50
    return index[vec_lda]
