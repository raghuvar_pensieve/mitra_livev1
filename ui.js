var express         =   require('express');
var path            =   require('path');
var favicon         =   require('serve-favicon');
var logger          =   require('morgan');
var cookieParser    =   require('cookie-parser');
var bodyParser      =   require('body-parser');
var session         =   require("express-session");
var cors            =   require('cors');

// Config variable
global.config = require('konfig')();


var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


// use morgan to log requests to the console
app.use(logger('dev'));
app.use(cors());

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({resave: true, saveUninitialized: true, secret: 'PENSIEVERANDOMSECRETHERE', cookie: { maxAge: 5400000 }}  ));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));

//app.use will allow us to access the public directory of the app and referencing the content inside it. 
//
//
// app.get('/', function (req, res) {
//   res.send('Hello World! sss');
// });
//
//
// app.get('/testview', function (req, res) {
//     res.render('testview', { title: 'Hey', message: 'Hello there!'});
// });


require('./routes/index')(app);







// app.listen(3000, function () {
//     console.log('Example app listening on port 3000!');
// });



app.listen(config.app.port, function () {
  console.log('Example app listening on port - ',config.app.port);
});

// server.listen(config.app.port);
module.exports = app;
