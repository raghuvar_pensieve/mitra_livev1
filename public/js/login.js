var loginApp = angular.module('ui.bootstrap.login', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);
loginApp.controller('loginController', function($scope, $window, $http) {

    $scope.login = function() {
        var data =
            JSON.stringify({
                username: $scope.username,
                password: $scope.password
            });
        $http.post("/login", data).success(function(data, status) {
            if (data == "true") {
                if($('#caseid').text()==0)
                {
                    $window.location.href = '/';
                }
                else
                {
                    data_type = $('#data_type');
                    console.log("data type on login page ::: " + data_type);
                    $window.location.href = '/relation/' + data_type + '/' +$('#caseid').text();
                }
            } else {
                $scope.error = data;
            }
        })
    }
});