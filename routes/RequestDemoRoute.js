module.exports = function(app) {

    var RequestDemoController = require('../controllers/RequestDemoController');

    app.get('/requestDemo', RequestDemoController.loadPage);
    app.post('/submitDemoData', RequestDemoController.submitDemoData);
};