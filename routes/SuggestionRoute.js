module.exports = function(app) {

    var SuggestionController = require('../controllers/SuggestionController');

    app.post('/suggestive_phrases', SuggestionController.suggest);

};