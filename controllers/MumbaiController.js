var request = require('request');
const util = require('util')

var production = '52.76.250.165'
var staging = '52.220.121.25'

function home (req, res, next) {

    // return res.send("WELCOME TO REST API UPDATE");
    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('home/mumbai_home', {
        loggedin:loggedin,
        username:req.session.user,
        title: 'Mitra'
    })

}

function search (req, res, next) {

    // return res.send("WELCOME TO REST API UPDATE");
    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('search/mumbai_search', {
        loggedin:loggedin,
        username:req.session.user,
        title: 'Mumbai SC Search',
        searchterm: req.params.searchterm
    })
}


function mumbai_search (req, res, next) {
 
    var posturl = req.body.url;

    var postbody = JSON.parse(req.body.body);
    var url         = 'http://' + production + ':3000/'+ posturl;
//    var url         = 'http://127.0.0.1' + ':3000/'+ posturl;
    var postData    = postbody;



    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };

    request(options, function (err, resp, body) {
        if(err) {
            res.send(err);
        } else {
            res.send(body);
        }
    })
}

module.exports = {
    search: search,
    home : home,
    mumbai_search : mumbai_search
};