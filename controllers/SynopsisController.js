var request = require('request');
const util = require('util')

production_api = "52.76.250.165"
function detail (req, res, next) {
    if(req.session.username == null)
    {
        res.redirect('/login/0')
    }
    else
    {
        var loggedin = true
        res.render('synopsis/detail', {
            loggedin:loggedin,
            username:req.session.user,
            title: 'Synopsis'
        })
    }

}


function summarise (req, res, next) {

    var posturl = req.body.url;

    var postbody = JSON.parse(req.body.body);
    var url         = 'http://' + production_api + ':3000/'+ posturl;

    var postData    = postbody;


    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };

    request(options, function (err, resp, body) {
        if(err) {

            res.send(err);
        } else {
            res.send(body);
        }
    })
}


module.exports = {
    detail: detail,
    summarise : summarise

};