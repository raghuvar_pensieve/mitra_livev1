var request = require('request');
var ip = '52.76.250.165' //API server
var port = 3000
function loginhome (req, res, next) {

    // return res.send("WELCOME TO REST API UPDATE");

    res.render('account/login', {
        title: 'Login',
        caseid: req.params.id,
        data_type : req.params.data_type
    })

}


function login (req, res, next) {
    console.log(req.body);
    var postData = req.body;
    var url  = 'http://' + ip + ':' + port + '/login';
    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };
    console.log(options);
    request(options, function (err, resp, body) {
        if(err) {
            res.send("An error occured. Please try again later");
        } else {
            if(typeof(body)==="string")
            {
                res.send(body.toString());
            }
            else
            {
                req.session.username = body.email;
                req.session.user = body.name;
                res.send("true");
            }
        }
    })
}

function logout (req, res, next) {
    //destroy session
    req.session.destroy();

     res.render('home/home', {
        title: 'Mitra'
    })
}

function changepassword (req, res, next)
{
    if(req.session.username == null)
    {
        res.redirect('/')
    }
    else
    {
        var loggedin = true;
         res.render('account/changepassword', {
            loggedin:loggedin,
            username:req.session.user,
            title: 'Change Password',
        })
    }
}

function changepasswordvalidate(req, res, next)
{
    console.log(req.body);
    req.body['username'] = req.session.username;
    var postData = req.body;
    var url  = 'http://' + ip + ':' + port + '/changepassword';
    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };
    console.log(options);
    request(options, function (err, resp, body) {
        if(err) {
            res.send("An error occured. Please try again later");
        } else {
            if(body == "true")
            {
                res.send("true")
            }
            else
                res.send(body.toString());
        }
    })
}

function signup(req, res, next)
{
    res.render('account/signup', {
        title: 'Register with us',
    })
}

function signupvalidate(req, res, next)
{
    console.log(req.body);
    var postData = req.body;
    var url  = 'http://' + ip + ':' + port + '/signup';
    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };
    console.log(options);
    request(options, function (err, resp, body) {
        if(err) {
            res.send("An error occured. Please try again later");
        } else {
            console.log('body recived')
            res.send(body.toString());
        }
    })
}

//Adding By Raghuvar


function forgotpassword(req, res, next)
{
    res.render('account/forgotpassword', {
        title: 'Forgot Password',
    })
}

function forgotpasswordvalidate(req, res, next)
{
    console.log(req.body);
    var postData = req.body;
    var url  = 'http://' + ip + ':' + port + '/forgotpassword';
    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };
    console.log(options);
    request(options, function (err, resp, body) {
        if(err) {
            res.send("An error occured. Please try again later");
        } else {
            res.send(body.toString());
        }
    })
}

function verifyregistration(req, res, next)
{
    var access_token = req.params.token;
    var email = req.params.email;
    console.log(access_token+","+email);
    var postData = {'access_token':access_token,'email':email};
    var url  = 'http://' + ip + ':' + port + '/verifyregistration';
    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };
    console.log(options);
    request(options, function (err, resp, body) {

        res.render('account/verify', {
        title: 'Account Verification',
        message: body.toString()
    })
    })
}

function myAccount(req,res){
        if(req.session.username == null){
            res.redirect('/login/0');
        }
        else{
            res.render('myAccount/myAccount',{'username':req.session.username,'user':req.session.user});    
        }
        
}

function notePad(req,res){
        if(req.session.username == null){
            res.redirect('/login/0');
        }
        else{
            res.render('notePad/notePad',{'username':req.session.user});    
        }
        
}

function saveNote(req,res){
    console.log(req.body);
    var url = 'http://' + ip + ':3000/saveNote';
    var postData = {
        content: req.body.content,
        pageName: req.body.pageName,
        tag: req.body.tag,
        email: req.session.username
    }
    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };
    request(options, function (err, resp, body) {
        if(err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
}

function loadNote(req,res){
    if(req.session.username == null){
            res.redirect('/login/0');
    }
    else{
        var url = 'http://' + ip + ':3000/loadNote/'+req.params.tag;
        var postData = {
            email: req.session.username
        }
        var options = {
            url: url,
            body: postData,
            method: 'post',
            json: true
        };
        request(options, function (err, resp, body) {
            if(err) {
                res.send(err);
            } else {
                res.render('notePad/loadNote',{'username':req.session.user,'content':body.content});
            }
        });       
    }
    
    
}

module.exports = {
    loginhome: loginhome,
    login: login,
    logout:logout,
    changepassword:changepassword,
    changepasswordvalidate:changepasswordvalidate,
    signup:signup,
    signupvalidate:signupvalidate,
    forgotpassword:forgotpassword,
    forgotpasswordvalidate:forgotpasswordvalidate,
    verifyregistration:verifyregistration,
    myAccount:myAccount,
    notePad:notePad,
    saveNote: saveNote,
    loadNote: loadNote
};