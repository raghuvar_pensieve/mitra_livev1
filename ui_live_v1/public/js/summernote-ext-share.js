(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('jquery'));
    } else {
        factory(window.jQuery);
    }
}(function ($) {
    $.extend($.summernote.options, {
        share: {}
    });

    // Extend plugins for adding templates
    $.extend($.summernote.plugins, {
        /**
         * @param {Object} context - context object has status of editor.
         */
        'share': function (context) {
            var ui = $.summernote.ui;
            var options = context.options.share;
            var defaultOptions = {
                label: 'Share',
                tooltip: 'Share',
                path: '',
                list: {}
            };

            // Assign default values if not supplied
            for (var propertyName in defaultOptions) {
                if (options.hasOwnProperty(propertyName) === false) {
                    options[propertyName] = defaultOptions[propertyName];
                }
            }

            // add share button
            context.memo('button.share', function () {
                // initialize list
                var htmlDropdownList = '';
                for (var htmlShare in options.list) {
                    if (options.list.hasOwnProperty(htmlShare)) {
                        htmlDropdownList += '<li><a style="color:#ffffff; background-color:#042f4f;" href="#" data-value="' + htmlShare + '">' + options.list[htmlShare] + '</a></li>';                                                
                    }
                }

                // create button
                var button = ui.buttonGroup([
                    ui.button({
                        className: 'dropdown-toggle',
                        contents: '<i class="fa fa-share-alt" />'+ '<span class="share"/> ' + options.label + ' <span class="caret"></span>',
                        tooltip: options.tooltip,
                        data: {
                            toggle: 'dropdown'
                        }
                    }),
                    ui.dropdown({
                        className: 'dropdown-share',
                        items: htmlDropdownList,
                        click: function (event) {
                            event.preventDefault();                          
                            var content = "";

// Check Whether you want to share the content through NotePad Directly or through Synopsis Editor
                            if (event.target.baseURI == "http://localhost:7777/synopsis"){
                                content = $("#summernote").summernote("code");                              
                            }
                            else {
                                content = $("#notePad").summernote("code");                              
                            }


/* Gmail API Starts from here */                                                        
                            if (event.target.innerHTML == 'Gmail'){

                              var person = prompt("Please enter your email address", "");
                              if (person != ""){
                                swal({
                                      title : "Sweet",
                                      text : "Send Messages",
                                      button : "success"
                                });
                              }
                              var urls = "/ShareGmail";
                              var postData = {
                                  content : content,
                                  person : person,
                              };

                              $.ajax({
                                type:'POST',
                                url: urls,
                                data: postData,
                                dataType: "json"
                              }).done(function(response){
                                  console.log(response);
                              });
                            }
/* Gmail API Ends here */                            


/* LinkedIn API Starts from here */                            
                            else if (event.target.innerHTML == 'LinkedIn'){
                              var text = $("#notePad").summernote("code").replace(/<\/?[^>]+(>|$)/g, "");
                              var postData = {
                                "comment" : text,
                                "visibility" : {
                                "code" : "anyone"
                                }
                              }
                              function onSuccess(data){
                                console.log(data);
                              }

                              function onError(error){
                                console.log(error);
                              }

                              //function returnValue(){
                              //  var text = $("#notePad").summernote("code");
                              //  return text;
                              //}

                              

                              IN.API.Raw('/people/~/shares?format=json')
                              .method("POST")
                              .body(JSON.stringify(postData))
                              .result(onSuccess)
                              .error(onError);    
                              

                              //content = $("#notePad").summernote("code");
/*                                  function returnValue(){
                                    var text = $("#notePad").summernote("code");
                                    return text;
                                  }

                              var fullContent = returnValue();
                              console.log(fullContent);
/*                              function onLinkedInLoad(){
                                IN.User.authorize(shareContent);
                              }

                              function  onSuccess(data) {
                                  console.log(data);
                                }

                                function onError(error) {
                                  console.log(error);
                                }
                                

                                function shareContent() {
                                console.log(content);
                                var postData = {
                                  "comment" : "idk it",
                                  "visibility" : {
                                    "code" : "anyone"
                                    }
                                };
                                }

                              //console.log(content);

/*                              urls = 'https://api.linkedin.com/v1/people/~/shares?format=json';
                              var postData = {                                
                                    "title" : "Is this right",
                                    "description" : content                              
                              };
                              $.ajax({
                                  method : 'POST',
                                  url : urls,
                                  content : postData,
                                  dataType : "jsonp",
//                                  "x-li-format" : "json",
                                  "Content-Type" : "application/json"
                              }).done(function (response) {
                                  console.log(response);
                              });                             */ 
                            }
/* LinkedIn API Ends here */                            


/* WhatsApp API Starts from here */                            
                            else if (event.target.innerHTML == 'WhatsApp'){
                              //alert("WhatsApp API should be called");
                              var urls = '/ShareWhatsApp';
                              var postData = {
                                content : content
                              };

                              $.ajax({
                                  method : 'POST',
                                  url : urls,
                                  data : postData,
                                  dataType : "json"
                              }).done(function (response) {
                                  console.log(response);
                              });
                            }
/* WhatsApp API Ends here */                                                        
                        }
                    })
                ]);

                // create jQuery object from button instance.
                return button.render();
            });
        }
    });
}));
