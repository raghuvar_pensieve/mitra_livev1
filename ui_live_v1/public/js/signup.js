var signupApp = angular.module('ui.bootstrap.signup', ['ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ngToast']);
signupApp.controller('signupController', function($scope, $window, $http, ngToast) {
    
    ngToast.create({
      className: 'alert alert-info',
      content: 'Access Code is Mitra30',
      dismissOnTimeout: false
    });

    $scope.signup = function() {
        
        if($scope.newpassword == $scope.repassword)
        {
            {
                $scope.error = "";
                var data =
                JSON.stringify({
                    name:$scope.name,
                    email:$scope.email,
                    phone:$scope.phone,
                    company:$scope.company,
                    newpassword: $scope.newpassword,
                    repassword: $scope.repassword,
		    accesscode: 'Mitra30',
                    memtype: $scope.memtype
                });
                $http.post("/signupvalidate", data).success(function(data, status) {
                    if (data == "true") {
                            alert("An email has been sent to your email id. Please use the link to complete the registration process")
                            $window.location.href = '/';
                    }
                    else {
                        alert(data);
                        $window.location.href = '/login/0';
                    }
                    
                });    
            }
                        
        }
        else
        {
            $scope.error = "The passwords don't match";
        }
        
    }
});