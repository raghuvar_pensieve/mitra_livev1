var request = require('request');
var ip = '52.76.250.165' //API server
var port = 3000


var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;
var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;

//var User       = require('./Passportusers');

// load the auth variables
var configAuth = require('./auth'); // use this one for testing

module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });
    // used to deserialize the user

    passport.deserializeUser(function(id, done) {
        //console.log(req._passport.session);
        console.log(id);

        var pid = new Object();
        pid['id'] = id;
        //console.log(pid);
            var options = {
                method: "post",
                url: "http://localhost:3000/google/getUser",
                body: pid,
                dataType: 'json'
                //json: true
            }
            console.log(options);

            request(options, function(err, resp){
                //console.log(resp);
                console.log(resp.body);
                if(err){
                    throw new Error(err);
                }
                else{
                   return done(null, false);
                }
            })
    });


    passport.use(new GoogleStrategy({

        clientID        : configAuth.google.clientID,
        clientSecret    : configAuth.google.clientSecret,
        callbackURL     : configAuth.google.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
        function(req, accessToken, refreshToken, profile, done){
            var postData = profile;
            var options = {
                method: "post",
                url: "http://localhost:3000/google",
                body: postData,
                json: true 
            }
            console.log(options);
            //console.log(req.session);

            var name = profile['displayName'];
            var profile_id = profile['id'];
            var email = profile['emails'][0].value;

            request(options, function(err, resp, item){
                //console.log(resp.body);
                if(err){
                    throw new Error(err);
                }
                else{
                    return done(null, resp.body);
                }
            })

            // Get the user for deserialize
            //var id = id;
            //console.log(id);


        }

))

/*
function login (req, res, next) {
    console.log(req.body);
    var postData = req.body;
    var url  = 'http://' + ip + ':' + port + '/login';
    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };
    console.log(options);
    request(options, function (err, resp, body) {
        if(err) {
            res.send("An error occured. Please try again later");
        } else {
            if(typeof(body)==="string")
            {
                res.send(body.toString());
            }
            else
            {
                req.session.username = body.email;
                req.session.user = body.name;
                res.send("true");
            }
        }
    })
}

*/

// LinkedIn Strategy 

    passport.use(new LinkedInStrategy({

        clientID        : configAuth.linkedin.clientID,
        clientSecret    : configAuth.linkedin.clientSecret,
        callbackURL     : configAuth.linkedin.callbackURL,
        authorizationURL: 'https://www.linkedin.com/oauth/v2/authorization',
        tokenURL: 'https://www.linkedin.com/oauth/v2/accessToken',
        scope: ['r_emailaddress', 'r_basicprofile'],
//        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done, user) {
        //console.log(req);
        // asynchronous
/*        var username = req.session.user;
        if(req.session.username == null){
            res.redirect('/login/0');
        }
        else{
            res.render('myAccount/myAccount',{'username':req.session.username,'user':req.session.user});    
        }        */
        process.nextTick(function() {
            
            return done();
            // check if the user is already logged in
            /*if (!req.user) {

                User.findOne({ 'linkedin.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        // if there is a user id already but no token (user was linked at one point and then removed)
                        if (!user.linkedin.token) {
                            user.linkedin.token = token;
                            user.linkedin.name  = profile.displayName;
                            user.linkedin.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                            user.save(function(err) {
                                if (err)
                                    return done(err);
                                    
                                return done(null, user);
                            });
                        }

                        return done(null, user);
                    } else {
                        var newUser          = new User();

                        newUser.linkedin.id    = profile.id;
                        newUser.linkedin.token = token;
                        newUser.linkedin.name  = profile.displayName;
                        newUser.linkedin.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                        newUser.save(function(err) {
                            if (err)
                                return done(err);
                                
                            return done(null, newUser);
                        });
                    }
                });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user               = req.user; // pull the user out of the session

                user.linkedin.id    = profile.id;
                user.linkedin.token = token;
                user.linkedin.name  = profile.displayName;
                user.linkedin.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                user.save(function(err) {
                    if (err)
                        return done(err);
                        
                    return done(null, user);
                });

            }
    */
        });

    }));

}
