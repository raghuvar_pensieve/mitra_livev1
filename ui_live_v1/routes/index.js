module.exports = function (app) {

    app.get('/welcome', function (req, res, next) {
        return res.send("WELCOME TO PENSIEVE :)");
    });


    require("./AccountRoute")(app);
    require("./CaseRoute")(app);
    require("./company")(app);
    require("./Dashboard")(app);
    require("./DelhiRoute")(app);
    require("./ElasticSearchRoute")(app);
    require("./HomeRoute")(app);
    require("./MumbaiRoute")(app);
    require("./PolicyRoute")(app);
    require("./PricingRoute")(app);
    require("./RelationRoute")(app);
    require("./RequestDemoRoute")(app);
    require("./SearchRoute")(app);
    require("./ShareRoute")(app);    
    require("./SubscribeRoute")(app);
    require("./SuggestionRoute")(app);
    require("./SynopsisRoute")(app);
    require("./TaxRoute")(app);
    require("./USRoute")(app);
};

// Added ShareRoute for Share Button (By Raghuvar) (Arranged alphabetically)