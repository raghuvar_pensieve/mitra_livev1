module.exports = function(app) {

    var AccountController = require('../controllers/AccountController');

    app.get('/login/:id', AccountController.loginhome);
    app.post('/login', AccountController.login);
    app.get('/logout', AccountController.logout);
    app.get('/changepassword', AccountController.changepassword);
    app.post('/changepasswordvalidate', AccountController.changepasswordvalidate);
    app.get('/signup', AccountController.signup);
    app.post('/signupvalidate', AccountController.signupvalidate);
    app.get('/forgotpassword', AccountController.forgotpassword);
    app.post('/forgotpasswordvalidate', AccountController.forgotpasswordvalidate);
    app.get('/verifyregistration/:email/:token',AccountController.verifyregistration);
    app.get('/myAccount', AccountController.myAccount);
    app.get('/notePad', AccountController.notePad);
    app.get('/loadNote/:tag', AccountController.loadNote);
    app.post('/saveNote', AccountController.saveNote);
};
