module.exports = function(app) {

    var TaxController = require('../controllers/TaxController');

    app.get('/tax/search/:searchterm', TaxController.search);
    app.get('/tax', TaxController.home) ;
    app.post('/tax_search', TaxController.tax_search);
};